<?

$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.countdown.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.countdown-ru.js');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/jquery.countdown.css');

CModule::IncludeModule("iblock");

$iblocks = GetIBlockList("services", "BANNERS");
$arIBlock = $iblocks->GetNext();

$arBanners = array();

if ($arIBlock) {
	$db_enum_list = CIBlockProperty::GetPropertyEnum("BTYPE", Array(), Array("IBLOCK_ID" => $arIBlock['ID'], "XML_ID" => "top"));
	$arPropertyType = $db_enum_list->GetNext();
	
	if ($arPropertyType) {
		$arFilter = Array("PROPERTY_BTYPE_VALUE" => $arPropertyType['VALUE']);
		$banners = GetIBlockElementList($arIBlock['ID'], false, Array("SORT" => "ASC"), 0, $arFilter);
		$k = -1;
		while ($arBanner = $banners->GetNext()) {
			$k++;
			$arBanners[] = GetIBlockElement($arBanner['ID']);
			$arBanners[$k]['END_DATETIME'] = strtotime($arBanners[$k]['PROPERTIES']['END_DATETIME']['VALUE']);
		}
	}
}

?>
<!-- Баннер -->
<script type="text/javascript">
	$(function() {
		$.countdown.setDefaults($.countdown.regional['ru']);
		
		var longWayOff = [];
		
		<? foreach ($arBanners as $k => $arBanner): ?>
			longWayOff[<?= $k; ?>] = new Date(
				<?= date("Y", $arBanner['END_DATETIME']); ?>,
				<?= date("m", $arBanner['END_DATETIME']) - 1; ?>,
				<?= date("d", $arBanner['END_DATETIME']); ?>,
				<?= date("H", $arBanner['END_DATETIME']); ?>,
				<?= date("i", $arBanner['END_DATETIME']); ?>,
				<?= date("s", $arBanner['END_DATETIME']); ?>
			);
				
			$('#clock-<?= $k; ?>').countdown({
				until: longWayOff[<?= $k; ?>], 
				format: 'DHMS', 
				compact: true,
				//description: 'Jusqu\'au décollage'
			}); 
		<? endforeach; ?>
		
		var current_banner_index = 0;
		
		function changeTopBanner() {
			var $b = $('#banner');
			var $bs = {
				i: $b.find('.banners > .item'),
				bckg: $b.find('.banner-backg > .item'),
				timer: $b.find('.banner-time > .item'),
				url: $b.find('.item.banner-but'),
			};
			
			//alert(current_banner_index + ' ' + $bs.length + ' ' + $bs_bckg.length + ' ' + $bs_timer.length + ' ' + $bs_url.length);

			for (var x in $bs)
				$bs[x].eq(current_banner_index).animate({opacity: 0});

			current_banner_index++;
			
			if (current_banner_index === $bs.i.length)
				current_banner_index = 0;
			
			for (var x in $bs)
				$bs[x].eq(current_banner_index).animate({opacity: 1});
		}
		
		setInterval(changeTopBanner, 5000);
	});
</script>
<div id="banner">
	<div class="banners">
		<? foreach ($arBanners as $k => $arBanner): ?>
			<img class="item" <? if ($k): ?>style="opacity: 0;"<? endif; ?> src="<?= CFile::GetPath($arBanner['DETAIL_PICTURE']) ?>" alt="<?= $arBanner['NAME'] ?>"  />
		<? endforeach; ?>
	</div>
	<div class="banner-backg">
		<? foreach ($arBanners as $k => $arBanner): ?>
			<div class="item" <? if ($k): ?>style="opacity: 0;"<? endif; ?> >
				<?= $arBanner['PREVIEW_TEXT'] ?>
			</div>
		<? endforeach; ?>
	</div>
	<div class="banner-time">
		<span>осталось времени:</span>
		<? foreach ($arBanners as $k => $arBanner): ?>
			<div id="clock-<?= $k; ?>" class="item" <? if ($k): ?>style="opacity: 0;"<? endif; ?> ></div>
		<? endforeach; ?>
	</div>
	<? foreach ($arBanners as $k => $arBanner): ?>
		<a href="<?= $arBanner['PROPERTIES']['URL1']['VALUE'] ?>" class="item banner-but" <? if ($k): ?>style="opacity: 0;"<? endif; ?> >
			<span>ОФОРМИТЬ ПРЕДЗАКАЗ</span>
		</a>
	<? endforeach; ?>
</div>
<!-- ///Баннер -->