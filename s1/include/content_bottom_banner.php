<?
CModule::IncludeModule("iblock");

$iblocks = GetIBlockList("services", "BANNERS");
$arIBlock = $iblocks->GetNext();

$arBanners = array();

if ($arIBlock) {
	$db_enum_list = CIBlockProperty::GetPropertyEnum("BTYPE", Array(), Array("IBLOCK_ID" => $arIBlock['ID'], "XML_ID" => "bottom"));
	$arPropertyType = $db_enum_list->GetNext();

	if ($arPropertyType) {
		$arFilter = Array("PROPERTY_BTYPE_VALUE" => $arPropertyType['VALUE']);
		$banners = GetIBlockElementList($arIBlock['ID'], false, Array("SORT" => "ASC"), 0, $arFilter);
		$k = -1;
		while ($arBanner = $banners->GetNext()) {
			$k++;
			$arBanners[] = GetIBlockElement($arBanner['ID']);
			$arBanners[$k]['END_DATETIME'] = strtotime($arBanners[$k]['PROPERTIES']['END_DATETIME']['VALUE']);
		}
	}
}
?>
<!-- Обзор игр -->
<h1 class="caption end">Обзор игр</h1>
<div id="review">
	<? foreach ($arBanners as $k => $arBanner): ?>
		<!-- <?= $k; ?>-ый блок -->
		<div class="reliz">
			<div class="back-ground">
				<p><?= $arBanner['PREVIEW_TEXT'] ?></p>
				<span><a href="<?= $arBanner['PROPERTIES']['URL1']['VALUE'] ?>" >обзор игры</a></span>
			</div>
			<img src="<?= CFile::GetPath($arBanner['PREVIEW_PICTURE']) ?>" alt="<?= $arBanner['NAME'] ?>" />
		</div>
		<!-- ///<?= $k; ?>-ый блок -->
	<? endforeach; ?>
</div>
<!-- ///Обзор игр -->