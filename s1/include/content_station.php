<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<script type="text/javascript">
    $(function() {
        function changeTab($this) {
            $this = $($this);

            $this.addClass("act").siblings("a").removeClass("act");

            var $p = $this.parents('.page-prod:first');

            var $t = {
                NEWPRODUCT: $p.find('.tables.NEWPRODUCT'),
                SALELEADER: $p.find('.tables.SALELEADER'),
            };

            for (var x in $t) {
                if ($this.hasClass(x)) {
                    $t[x].show();
                } else {
                    $t[x].hide();
                }
            }
        }

        function changeProdNext($this) {
            $this = $($this);
            var $p = $this.parents('.page-prod:first');

            var $list = $p.find('.tables.NEWPRODUCT').is(':visible')
                    ? $p.find('.tables.NEWPRODUCT')
                    : $p.find('.tables.SALELEADER');

            var $items = $list.find('.products-sample');
            var l = $items.length;

            $items.eq(0).hide(
                    'normal',
                    function() {
                        $items.eq(0).insertAfter($items.eq(l - 1));

                        $items = $list.find('.products-sample');
                        $items.eq(1).show('fast');
                    });
        }

        function changeProdPrev($this) {
            $this = $($this);
            var $p = $this.parents('.page-prod:first');

            var $list = $p.find('.tables.NEWPRODUCT').is(':visible')
                    ? $p.find('.tables.NEWPRODUCT')
                    : $p.find('.tables.SALELEADER');

            var $items = $list.find('.products-sample');
            var l = $items.length;

            $items.eq(1).hide(
                    'normal',
                    function() {
                        $items.eq(l - 1).insertBefore($items.eq(0));

                        $items = $list.find('.products-sample');
                        $items.eq(0).show('fast');
                    });
        }


        $(".sort a").click(function() {
            changeTab(this);
        });

        $(".switch.one").click(function() {
            changeProdPrev(this);
        });

        $(".switch.two").click(function() {
            changeProdNext(this);
        });

        changeTab($(".sort a").first());
    });
</script>
<?
$ar_SECTION_NAME = array("XBOX", "PlayStation");

CModule::IncludeModule("iblock");
CModule::IncludeModule("catalog");

$iblocks = GetIBlockList("catalog", "catalog");
$arIBlock = $iblocks->GetNext();
?>
<? foreach ($ar_SECTION_NAME as $SECTION_NAME): ?>
<?
$arProducts = array();
$arProducts['NEWPRODUCT'] = array();
$arProducts['SALELEADER'] = array();

if ($arIBlock) {
$db_enum_list = CIBlockProperty::GetPropertyEnum("NEWPRODUCT", Array(), Array("IBLOCK_ID" => $arIBlock['ID'], "XML_ID" => "true"));
$arPropertyType_NEWPRODUCT = $db_enum_list->GetNext();

$db_enum_list = CIBlockProperty::GetPropertyEnum("SALELEADER", Array(), Array("IBLOCK_ID" => $arIBlock['ID'], "XML_ID" => "true"));
$arPropertyType_SALELEADER = $db_enum_list->GetNext();

if ($arPropertyType_NEWPRODUCT && $arPropertyType_SALELEADER) {
// выберем папки из информационного блока $BID и раздела $ID
$sections = GetIBlockSectionList($arIBlock['ID'], false, Array("sort" => "asc"), 0, Array("NAME" => "%{$SECTION_NAME}%"));
while ($arSection = $sections->GetNext()) {
$section_id = $arSection['ID'];

$arFilter = Array("PROPERTY_NEWPRODUCT_VALUE" => $arPropertyType_NEWPRODUCT['VALUE']);
$products = GetIBlockElementList($arIBlock['ID'], $section_id, Array("SORT" => "ASC"), 0, $arFilter);
while ($arProduct = $products->GetNext()) {
$arTMP = GetIBlockElement($arProduct['ID']);
$arTMP['AR_PRICE'] = CCatalogProduct::GetOptimalPrice($arProduct['ID']);
$arTMP['PRICE'] = $arTMP['AR_PRICE']['PRICE']['PRICE'];
$arTMP['DISCOUNT_PRICE'] = $arTMP['AR_PRICE']['DISCOUNT_PRICE'];
$arProducts['NEWPRODUCT'][] = $arTMP;
}

$arFilter = Array("PROPERTY_SALELEADER_VALUE" => $arPropertyType_SALELEADER['VALUE']);
$products = GetIBlockElementList($arIBlock['ID'], $section_id, Array("SORT" => "ASC"), 0, $arFilter);
while ($arProduct = $products->GetNext()) {
$arTMP = GetIBlockElement($arProduct['ID']);
$arTMP['AR_PRICE'] = CCatalogProduct::GetOptimalPrice($arProduct['ID']);
$arTMP['PRICE'] = $arTMP['AR_PRICE']['PRICE']['PRICE'];
$arTMP['DISCOUNT_PRICE'] = $arTMP['AR_PRICE']['DISCOUNT_PRICE'];
$arProducts['SALELEADER'][] = $arTMP;
}
}
}
}
?>
<div class="product-content yes">
    <!-- Страница товара1 -->
    <div class="page-prod">
        <div class="sort">
            <img src="/bitrix/templates/arcada_artem/images/<?= $SECTION_NAME; ?>.png" alt="<?= $SECTION_NAME; ?>"><h1 class="sort_caption"><?= $SECTION_NAME; ?></h1>
            <a href="javascript:void(0);" class="SALELEADER act" >Лидеры продаж</a>
            <a href="javascript:void(0);" class="NEWPRODUCT" >Новинки</a>
        </div>
        <div class="switch one"><a href="javascript:void(0);" class="prev"></a></div>
        <div class="switch two"><a href="javascript:void(0);" class="next"></a></div>
        <? foreach ($arProducts as $kk => $ar): ?>
        <div class="tables <?= $kk; ?>" <? if ($kk != 'SALELEADER'): ?>style="display: none;"<? endif; ?>>
             <? foreach ($ar as $k => $arProduct): ?>
                             <!-- <?= $k; ?>-ый блок -->
             <div class="products-sample" <? if ($k >= 3): ?>style="display: none;" <? endif; ?> >

             <div class="page-product-content">
                    <a href="<?= $arProduct['DETAIL_PAGE_URL'] ?>" title="<?= $arProduct["NAME"] ?>" >
                        <img class="product-cover" src="<?= CFile::GetPath($arProduct['DETAIL_PICTURE']) ?>" alt="<?= $arProduct['NAME'] ?>">
                    </a>
                    <div class="price_header">
                        <a href="<?= $arProduct['DETAIL_PAGE_URL'] ?>">
                            <?= trim($arProduct['NAME']) ?>
                        </a>
                    </div>
                    <div class="price-prod">
                        <p class="price-old">
                            <? if ($arProduct['PRICE'] != $arProduct['DISCOUNT_PRICE']): ?>
                            <span><?= $arProduct['PRICE']; ?></span> руб.
                            <? endif; ?>
                        </p>
                        <p class="price"><span><?= $arProduct['DISCOUNT_PRICE']; ?></span> руб.</p>
                        <div class="but-buy"><a href="/catalog/<?= $arProduct['IBLOCK_SECTION_ID'] ?>/<?= $arProduct["ID"]; ?>/?action=ADD2BASKET&id=<?= $arProduct['ID'] ?>"></a></div>
                    </div>
                </div>
            </div>
            <!-- ///<?= $k; ?>-ый блок -->
            <? endforeach; ?>
        </div>
        <? endforeach; ?>
    </div>
    <!-- ///Страница товара1 -->
</div>
<? endforeach; ?>