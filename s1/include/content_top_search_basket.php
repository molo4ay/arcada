<!-- Поисковик и корзина (блок для фильтра)-->
<div class="search-box"> 
	<?
	$APPLICATION->IncludeComponent(
		"bitrix:search.title", "catalog", Array(
			"NUM_CATEGORIES" => "1",
			"TOP_COUNT" => "5",
			"CHECK_DATES" => "N",
			"PAGE" => SITE_DIR . "search/",
			"CATEGORY_0_TITLE" => "Каталог",
			"CATEGORY_0" => array("iblock_catalog"),
			"CATEGORY_0_iblock_catalog" => array("all"),
			"CATEGORY_1_TITLE" => "Новости",
			"CATEGORY_1" => array("iblock_news"),
			"CATEGORY_1_iblock_news" => array("all"),
			"SHOW_INPUT" => "Y",
			"INPUT_ID" => "title-search-input",
			"CONTAINER_ID" => "search",
			"PRICE_CODE" => array("retail"),
			"CONVERT_CURRENCY" => "Y",
				
	    "ORDER" => "date",
		  "USE_LANGUAGE_GUESS" => "N",
			"SHOW_INPUT" => "Y",

//			"SHOW_OTHERS" => "Y",
//			"CATEGORY_OTHERS_TITLE" => "Прочее",
//			"SHOW_PREVIEW" => "Y",
//			"PREVIEW_WIDTH" => "75",
//			"PREVIEW_HEIGHT" => "75",
		)
	);
	?>
	<?
	$APPLICATION->IncludeComponent(
			"bitrix:sale.basket.basket.small", ".default", Array(
			"PATH_TO_BASKET" => SITE_DIR . "personal/cart/",
			"PATH_TO_ORDER" => SITE_DIR . "personal/order/",
		), false, Array(
			'0' => ''
		)
	);
	?>
</div>
<!-- ////Поисковик и корзина -->