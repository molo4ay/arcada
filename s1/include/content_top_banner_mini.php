<?
CModule::IncludeModule("iblock");

$iblocks = GetIBlockList("services", "BANNERS");
$arIBlock = $iblocks->GetNext();

$arBanners = array();

if ($arIBlock) {
	$db_enum_list = CIBlockProperty::GetPropertyEnum("BTYPE", Array(), Array("IBLOCK_ID" => $arIBlock['ID'], "XML_ID" => "mini"));
	$arPropertyType = $db_enum_list->GetNext();

	if ($arPropertyType) {
		$arFilter = Array("PROPERTY_BTYPE_VALUE" => $arPropertyType['VALUE']);
		$banners = GetIBlockElementList($arIBlock['ID'], false, Array("SORT" => "ASC"), 0, $arFilter);
		$k = -1;
		while ($arBanner = $banners->GetNext()) {
			$k++;
			$arBanners[] = GetIBlockElement($arBanner['ID']);
			$arBanners[$k]['END_DATETIME'] = strtotime($arBanners[$k]['PROPERTIES']['END_DATETIME']['VALUE']);
		}
	}
}
?>
<!-- Баннеры-мини -->

<script type="text/javascript">
	$(function() {
		var $bs_mini = $('#banner-mini').find('.item');
		
		$('#banner-mini').on("mouseenter", '.item', function() {
			$(this).find('.baner2-news').slideToggle("slow");
			$(this).parent().find('.item').removeClass('bm-active').addClass('bm-notactive');
			$(this).removeClass('bm-notactive').addClass('bm-active');
		});
		
		$('#banner-mini').on("mouseleave", '.item', function() {
			$(this).find('.baner2-news').slideToggle("fast");
			$(this).parent().find('.item').removeClass('bm-active').addClass('bm-notactive');
		});		
		 
		function changeMiniBanner() {
			var $bs = $('#banner-mini').find('.item');
			var l = $bs.length;

			$bs.eq(0).hide(
			'normal',
			function() {
				$bs.eq(l - 1).after($bs.eq(0));
				$bs.eq(l - 1).show();
			});

		}

		setInterval(changeMiniBanner, 5000);
	});
</script>
<div id="banner-mini"> 						 
	<ul> 							 
		<? foreach ($arBanners as $k => $arBanner): ?>
			<li class="item <? if ($k): ?>bm-notactive<? else: ?>bm-active<? endif; ?>" <? if ($k > 3): ?>style="display: none;"<? endif; ?>>
				<a href="<?= $arBanner['PROPERTIES']['URL1']['VALUE'] ?>" class="first" >
					<img src="<?= CFile::GetPath($arBanner['PREVIEW_PICTURE']) ?>" alt="<?= $arBanner['NAME'] ?>" />
				</a>
				<img class="bm-act" src="<?= SITE_TEMPLATE_PATH ?>/images/ban2-activ.png" alt="ban2-activ" />
				<a href="<?= $arBanner['PROPERTIES']['URL1']['VALUE'] ?>" class="baner2-news" ><?= $arBanner['PREVIEW_TEXT'] ?></a>
			</li>
		<? endforeach; ?>
	</ul>
</div>
<!-- ////Баннеры-мини -->