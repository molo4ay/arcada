<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Интернет-магазин игровых приставок \"Аркада\"");
?> 
<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH."/s1/include/content_top_banner.php"), false);?>
<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH."/s1/include/content_top_banner_mini.php"), false);?>
<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH."/s1/include/content_top_search_basket.php"), false);?>
<div class="clear"></div>
<?
$APPLICATION->IncludeComponent("bitrix:eshop.catalog.top", "recommend", array(
		"IBLOCK_TYPE_ID" => "catalog",
		"IBLOCK_ID" => "6",
		"ELEMENT_SORT_FIELD" => "RAND",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_COUNT" => "3",
		"FLAG_PROPERTY_CODE" => "SALELEADER",
		"OFFERS_LIMIT" => "5",
		"OFFERS_FIELD_CODE" => array(
				0 => "NAME",
				1 => "",
		),
		"OFFERS_PROPERTY_CODE" => array(
				0 => "COLOR",
				1 => "WIDTH",
				2 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_ORDER" => "asc",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id_top1",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "180",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_COMPARE" => "N",
		"PRICE_CODE" => array("retail"),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"OFFERS_CART_PROPERTIES" => array(
				0 => "COLOR",
				1 => "WIDTH",
		),
		"DISPLAY_IMG_WIDTH" => "130",
		"DISPLAY_IMG_HEIGHT" => "130",
		"SHARPEN" => "30"
	), false
);
?>					 						
<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH."/s1/include/content_station.php"), false);?>
<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH."/s1/include/content_bottom_banner.php"), false);?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>