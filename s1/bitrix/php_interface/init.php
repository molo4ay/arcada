<?php

// реализуем возможность загрузки из 1С множественной строки   
AddEventHandler("iblock", "OnAfterIBlockElementAdd", "UpdatePluralToProp_1с");
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "UpdatePluralToProp_1с");

function UpdatePluralToProp_1с(&$arFields) {
	// соберем массив свойств элемента и сделаем обработку строки которая может быть множественной
	// переменная для разделителей    
	$spacer = array(",", "\\");
	$properties = array();
	$db_props = CIBlockElement::GetProperty($arFields['IBLOCK_ID'], $arFields['ID'], array("sort" => "asc"), Array());
	while ($ob = $db_props->GetNext()) {
		// получим свойство в котором обнаружили разделитель

		//if ($ob['CODE'] != 'CML2_ATTRIBUTES') {
		if (in_array($ob['CODE'], array("ZHANR", "KATEGORIYA_SPECIAL"))) {
			$properties[$ob['ID']][] = $ob;
		}
	}

	foreach ($properties as $keyprops => $prop) {
		// удалим пустые значения
		foreach ($prop as $key_v => $values) {
			if (empty($values['VALUE'])) {
				unset($prop[$key_v]);
			}
		}

		if ((!empty($prop)) && (count($prop) <= 2)) {
			foreach ($prop as $key_pro => $v) {
				// пройдемся по массиву с разделителями
				foreach ($spacer as $spacer_value) {
					if (strpos($v['VALUE'], $spacer_value) !== false) {
						// разделим строку на массив
						//$v['VALUE'] = str_replace(" ","",$v['VALUE']);
						$v['VALUE'] = trim($v['VALUE']);
						$PROPERTY_VALUE = array();
						foreach (explode($spacer_value, $v['VALUE']) as $key_elem => $element_prop) {
							$element_prop = trim($element_prop);
							$PROPERTY_VALUE['n' . $key_elem] = array(
									'VALUE' => $element_prop
							);
						}

						CIBlockElement::SetPropertyValuesEx($arFields['ID'], $arFields['IBLOCK_ID'], array($keyprops => $PROPERTY_VALUE));
					}
				}
			}
		}
	}

	// printr($arFields);die;                 
}

?>
