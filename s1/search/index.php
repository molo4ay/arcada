<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поиск");
?>
<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH."/s1/include/content_top_search_basket.php"), false);?>
<? $_GET['iblock'] = 6; ?>
<?$APPLICATION->IncludeComponent("infospice.search:infospice.search.page", "", Array(
    "USE_SUGGEST" => "Y",    // Показывать подсказку с поисковыми фразами
    "SHOW_RATING" => "",    // Включить рейтинг
    "RATING_TYPE" => "",    // Вид кнопок рейтинга
    "PATH_TO_USER_PROFILE" => "",    // Шаблон пути к профилю пользователя
    "TEMPLATE_SECTION" => ".default",    // Шаблон категорий поиска
    "TEMPLATE_CATALOG" => "catalog",    // Шаблон списка товаров
    "TEMPLATE_IBLOCK" => "other",    // Шаблон для остальных результатов
    "PRICE_CODE" => array(    // Тип цены
        0 => "retail",
    ),
    "PAGE_IBLOCK_COUNT" => "9",    // Количество элементов на странице
    "RESTART" => "N",    // Искать без учета морфологии (при отсутствии результата поиска)
    "NO_WORD_LOGIC" => "N",    // Отключить обработку слов как логических операторов
    "USE_LANGUAGE_GUESS" => "Y",    // Включить автоопределение раскладки клавиатуры
    "CHECK_DATES" => "N",    // Искать только в активных по дате документах
    "USE_TITLE_RANK" => "N",    // При ранжировании результата учитывать заголовки
    "DEFAULT_SORT" => "rank",    // Сортировка по умолчанию
    "FILTER_NAME" => "",    // Дополнительный фильтр
    "CACHE_TYPE" => "A",    // Тип кеширования
    "CACHE_TIME" => "3600",    // Время кеширования (сек.)
    "CACHE_NOTES" => "",
    "DISPLAY_TOP_PAGER" => "Y",    // Выводить над результатами
    "DISPLAY_BOTTOM_PAGER" => "Y",    // Выводить под результатами
    "PAGER_TITLE" => "Результаты поиска",    // Название результатов поиска
    "PAGER_SHOW_ALWAYS" => "Y",    // Выводить всегда
    "PAGER_TEMPLATE" => "",    // Название шаблона
    "arrFILTER" => array(    // Ограничение области поиска
        0 => "iblock_catalog",
    ),
    "arrFILTER_iblock_catalog" => "all",    // Искать в информационных блоках типа "iblock_catalog"
    ),
    false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>