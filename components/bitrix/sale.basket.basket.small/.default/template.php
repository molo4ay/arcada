<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? 
$QTY = 0;
$SUM = 0;
foreach ($arResult["ITEMS"] as $v) {
	$QTY += $v["QUANTITY"];
	$SUM += $v["PRICE"] * $v["QUANTITY"];
}
?>
<div id="box">
	<p>Ваш заказ:</p>
	<div>
		<img class="bm-act" src="<?= SITE_TEMPLATE_PATH ?>/images/box_03.png" alt="box_03">
		<p><span><?= $SUM ? round($SUM) : '0'; ?></span> руб.</p>
		<p><span><?= $QTY ? $QTY : '0'; ?></span> шт.</p>
	</div>
	<a href="<?= strlen($arParams["PATH_TO_BASKET"]) ? $arParams["PATH_TO_BASKET"] : '/personal/cart/'; ?>">Перейти в корзину</a>
</div>