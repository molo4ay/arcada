<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? CJSCore::Init(array('fx', 'popup', 'window', 'ajax')); ?>

<?

function GetPValue($ar, $p) {
	$r = "";

	if (!empty($ar)) {
		foreach ($ar as $arProperties) {
			if ($arProperties["FIELD_NAME"] == $p || $arProperties["FIELD_ID"] == $p) {
				$r = $arProperties["VALUE"];
				break;
			}
		}
	}

	return $r;
}

//dump($arResult);
?>

<div id="order_form_div" class="order-checkout">

	<a href="/personal/cart/" name="order_form" title="Вернуться в корзину"></a>

	<NOSCRIPT>
	<div class="errortext"><?= GetMessage("SOA_NO_JS") ?></div>
	</NOSCRIPT>

	<?
	if (!$USER->IsAuthorized() && $arParams["ALLOW_AUTO_REGISTER"] == "N") {
		if (!empty($arResult["ERROR"])) {
			foreach ($arResult["ERROR"] as $v)
				echo ShowError($v);
		} elseif (!empty($arResult["OK_MESSAGE"])) {
			foreach ($arResult["OK_MESSAGE"] as $v)
				echo ShowNote($v);
		}

		include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/auth.php");
	} else {
		if ($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y") {
			if (strlen($arResult["REDIRECT_URL"]) > 0) {
				?>
				<script type="text/javascript">
					//window.top.location.href = '<?= CUtil::JSEscape($arResult["REDIRECT_URL"]) ?>';
				</script>
				<?
				die();
			} else {
				include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/confirm.php");
			}
		} else {
			?>
			<script type="text/javascript">
				function submitForm(val)
				{
					if (val != 'Y')
						BX('confirmorder').value = 'N';

					var orderForm = BX('ORDER_FORM');

					BX.ajax.submitComponentForm(orderForm, 'order_form_content', true);
					BX.submit(orderForm);

					return true;
				}

				function SetContact(profileId)
				{
					BX("profile_change").value = "Y";
					submitForm();
				}
			</script>
			<?
			if ($_POST["is_ajax_post"] != "Y") {
				?><form action="<?= $APPLICATION->GetCurPage(); ?>" method="POST" name="ORDER_FORM" id="ORDER_FORM">
						<?= bitrix_sessid_post() ?>
					<div id="order_form_content">
						<?
					} else {
						$APPLICATION->RestartBuffer();
					}
					if (!empty($arResult["ERROR"]) && $arResult["USER_VALS"]["FINAL_STEP"] == "Y") {
						foreach ($arResult["ERROR"] as $v)
							echo ShowError($v);
						?>
						<script type="text/javascript">
							top.BX.scrollToNode(top.BX('ORDER_FORM'));
						</script>
						<?
					}
					?>

					<? foreach ($arResult["PERSON_TYPE"] as $v): ?>
						<input type="hidden" id="PERSON_TYPE_<?= $v["ID"] ?>" name="PERSON_TYPE" value="<?= $v["ID"] ?>" />
						<? break; ?>
					<? endforeach; ?>
					<? foreach ($arResult["PAY_SYSTEM"] as $arPaySystem): ?>
						<input type="hidden" name="PAY_SYSTEM_ID" value="<?= $arPaySystem["ID"] ?>">
						<? break; ?>
					<? endforeach; ?>

					<!-- Индекс -->
					<input type="hidden" name="ORDER_PROP_4" value="101000">
					<!-- Индекс -->
					<input type="hidden" name="ORDER_PROP_6_val" value="Якутск, Саха /Якутия/ Респ, Россия">
					<input type="hidden" name="ORDER_PROP_6" value="128">

					<h2>ОФОРМЛЕНИЕ ЗАКАЗА</h2>
					<? if (!empty($arResult["ERROR"])): ?>
						<? foreach ($arResult["ERROR"] as $v): ?>
							<?= ShowError($v); ?>
						<? endforeach; ?>
					<? elseif (!empty($arResult["OK_MESSAGE"])): ?>
						<? foreach ($arResult["OK_MESSAGE"] as $v): ?>
							<?= ShowNote($v); ?>
						<? endforeach; ?>
						<? endif; ?>
					<div id="order_delivery" class="order_block">
						<p>Способ получения заказа:</p>&nbsp;&nbsp;&nbsp;
						<input type="hidden" name="BUYER_STORE" id="BUYER_STORE" value="<?= $arResult["BUYER_STORE"] ?>" />
						<? foreach ($arResult["DELIVERY"] as $delivery_id => $arDelivery): ?>
							<input type="radio" id="ID_DELIVERY_ID_<?= $arDelivery["ID"] ?>" name="<?= $arDelivery["FIELD_NAME"] ?>" value="<?= $arDelivery["ID"] ?>"<? if ($arDelivery["CHECKED"] == "Y") echo " checked"; ?> onclick="submitForm();" >
							<p><?= $arDelivery['NAME']; ?></p>
						<? endforeach; ?>
					</div>
					<div id="order_phone" class="order_block">
						<div>
							<label>E-mail:</label>
							<input  placeholder="Ваш e-mail" required="required" type="email" name="ORDER_PROP_2" value="<?= GetPValue($arResult["ORDER_PROP"]["USER_PROPS_Y"], "ORDER_PROP_EMAIL"); ?>" />
						</div>
						<div>
							<label>Телефон:</label>
							<input  placeholder="Ваш номер телефона" required="required" type="text" name="ORDER_PROP_3" value="<?= GetPValue($arResult["ORDER_PROP"]["USER_PROPS_Y"], "ORDER_PROP_PHONE"); ?>" />
						</div>
					</div>
					<div id="order_name" class="order_block">
						<div>
							<label>Имя:</label>
							<input placeholder="Ваше имя" required="required" type="text" name="ORDER_PROP_1" value="<?= GetPValue($arResult["ORDER_PROP"]["USER_PROPS_Y"], "ORDER_PROP_1"); ?>" />
						</div>
						<div>
							<label>Адрес (для доставки):</label>
							<input placeholder="Ваше адрес" required="required" type="text" name="ORDER_PROP_7" class="address"  value="<?= GetPValue($arResult["ORDER_PROP"]["USER_PROPS_Y"], "ORDER_PROP_7"); ?>" />
						</div>
					</div>
					<div id="order_info" class="order_block">
						<label>Дополнительная информация:</label>
						<textarea maxlength="1000" placeholder="Ваше сообщение" name="ORDER_DESCRIPTION"><?= isset($_REQUEST['ORDER_DESCRIPTION']) ? $_REQUEST['ORDER_DESCRIPTION'] : ""; ?></textarea>
					</div>
					<div id="order_offer" class="order_block">
						Гарантия: <span>12 месяцев</span>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="checkbox" name="confirmorder" value="Y" >
						С <a href="#" >условиями оферты</a> согласен
					</div>
					<p style="padding-left: 250px;">
						<input type="image" src="<?= SITE_TEMPLATE_PATH ?>/images/zacaz-tabl-pas.png" onClick="submitForm('Y');" name="submitbutton" alt="OK">
					</p>
					<input type="hidden" name="save" id="save" value="Y">

					<?
					if (strlen($arResult["PREPAY_ADIT_FIELDS"]) > 0)
						echo $arResult["PREPAY_ADIT_FIELDS"];
					?>
			<?
			if ($_POST["is_ajax_post"] != "Y") {
				?>
					</div>
					<input type="hidden" name="confirmorder" id="confirmorder" value="Y">
					<input type="hidden" name="profile_change" id="profile_change" value="N">
					<input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
					<!-- input type="button" name="submitbutton" onClick="submitForm('Y');" value="<?= GetMessage("SOA_TEMPL_BUTTON") ?>" class="bt3" -->
				</form>
				<? if ($arParams["DELIVERY_NO_AJAX"] == "N"): ?>
					<script type="text/javascript" src="/bitrix/js/main/cphttprequest.js"></script>
					<script type="text/javascript" src="/bitrix/components/bitrix/sale.ajax.delivery.calculator/templates/.default/proceed.js"></script>
				<? endif; ?>
				<?
			}
			else {
				?>
				<script type="text/javascript">
							top.BX('confirmorder').value = 'Y';
							top.BX('profile_change').value = 'N';
				</script>
				<?
				die();
			}
		}
	}
	?>
</div>