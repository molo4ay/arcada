<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

__IncludeLang(dirname(__FILE__) . '/lang/' . LANGUAGE_ID . '/' . basename(__FILE__));

$curPage = $GLOBALS['APPLICATION']->GetCurPage($get_index_page = false);

if ($curPage != SITE_DIR) {
	if (empty($arResult) || (!empty($arResult[count($arResult) - 1]['LINK']) && $curPage != urldecode($arResult[count($arResult) - 1]['LINK'])))
		$arResult[] = array('TITLE' => htmlspecialcharsback($GLOBALS['APPLICATION']->GetTitle(false, true)), 'LINK' => $curPage);
}

if (empty($arResult))
	return "";

$strReturn = '<a href="' . SITE_DIR . '">' . GetMessage('BREADCRUMB_MAIN') . '</a>';

$num_items = count($arResult);
for ($index = 0, $itemSize = $num_items; $index < $itemSize; $index++) {
	$prefix = "";
	
	$sid = $arResult[$index]["LINK"];
	if (!$index && strpos($sid, '/catalog/') !== false) {
		$sid = trim(substr($sid, strpos($sid, '/catalog/') + 9), "/");
		if (strpos($sid, '/') === false) {
			$res = CIBlockSection::GetByID($sid);
			if($ar_res = $res->GetNext()) {
				if ($sid = $ar_res['IBLOCK_SECTION_ID']) {
					$res = CIBlockSection::GetByID($sid);
					if($ar_res = $res->GetNext()) {
						$prefix = "<span class='prefix'>" . htmlspecialcharsex($ar_res['NAME']) . "</span>";
					}
				}
			}
		}
	}
	
	if ($prefix)
		$prefix .= '<span>&nbsp;&gt;&nbsp;</span>';
	
	$title = $prefix . htmlspecialcharsex($arResult[$index]["TITLE"]);

	$strReturn .= '<span>&nbsp;&gt;&nbsp;</span>';

	if ($arResult[$index]["LINK"] <> "" && $index != $itemSize - 1)
		$strReturn .= '<a href="' . $arResult[$index]["LINK"] . '" title="' . $title . '">' . $title . '</a> ';
	else
		$strReturn .= '<span>' . $title . '</span>';
}

return $strReturn;
?>