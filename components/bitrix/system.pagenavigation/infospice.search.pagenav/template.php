<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

if (!$arResult["NavShowAlways"]) {
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");
?>

<div class="navigation">
	<div class="navigation-pages">
		<? if ($arResult["NavPageNomer"] > 1): ?>
			<? if ($arResult["bSavePage"]): ?>
				<a id="navigation_1_previous_page" class="navigation-button" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>">&larr;</a>
			<? else: ?>
				<? if ($arResult["NavPageNomer"] > 2): ?>
					<a id="navigation_1_previous_page" class="navigation-button" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>">&larr;</a>
				<? else: ?>
					<a id="navigation_1_previous_page" class="navigation-button" href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">&larr;</a>
				<? endif ?>
			<? endif ?>
		<? else: ?>
			<a id="navigation_1_previous_page" class="navigation-button" href="">&larr;</a>
		<? endif ?>

		<? while ($arResult["nStartPage"] <= $arResult["nEndPage"]): ?>
			<? if ($arResult["nStartPage"] == $arResult["NavPageNomer"]): ?>
				<span class="nav-current-page"><?= $arResult["nStartPage"] ?></span>
			<? elseif ($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false): ?>
				<a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><?= $arResult["nStartPage"] ?></a>
			<? else: ?>
				<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"><?= $arResult["nStartPage"] ?></a>
			<? endif ?>
			<? $arResult["nStartPage"]++ ?>
		<? endwhile ?>

		<? if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]): ?>
			<a id="navigation_1_next_page" class="navigation-button" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>">Вперед</a>
		<? endif ?>

		<? if ($arResult["bShowAll"]): ?>
			<? if ($arResult["NavShowAll"]): ?>
				<a class="navigation-page-all" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>SHOWALL_<?= $arResult["NavNum"] ?>=0" rel="nofollow"><?= GetMessage('nav_paged') ?></a>
			<? else: ?>
				<a class="navigation-page-all" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>SHOWALL_<?= $arResult["NavNum"] ?>=1" rel="nofollow"><?= GetMessage('nav_all') ?></a>
			<? endif ?>
		<? endif ?>
	</div>
</div>
