<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<h1 class="capt">РЕКОМЕНДУЕМ</h1>
<!-- Рекоммендуем -->
<div class="suggest">
	<? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
		<? if (is_array($arItem)): ?>
			<?
			$PRICE = false;
			$DISCOUNT_PRICE = false;
			if (is_array($arItem["OFFERS"]) && !empty($arItem["OFFERS"])) {	 //if product has offers
				if (count($arItem["OFFERS"]) > 1) {
					$PRICE = $arItem["PRINT_MIN_OFFER_PRICE"];
				} else {
					foreach ($arItem["OFFERS"] as $arOffer):
						foreach ($arOffer["PRICES"] as $code => $arPrice):
							if ($arPrice["CAN_ACCESS"]):
								if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):
									$DISCOUNT_PRICE = $arPrice["PRINT_DISCOUNT_VALUE"];
								else:
									$PRICE = $arPrice["PRINT_VALUE"];
								endif;
							endif;
						endforeach; 
					endforeach;
				}
			}	else { // if product doesn't have offers
				if (count($arItem["PRICES"]) > 0 && $arItem['PROPERTIES']['MAXIMUM_PRICE']['VALUE'] == $arItem['PROPERTIES']['MINIMUM_PRICE']['VALUE']):
					foreach ($arItem["PRICES"] as $code => $arPrice):
						if ($arPrice["CAN_ACCESS"]):
							if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):
								$DISCOUNT_PRICE = $arPrice["PRINT_DISCOUNT_VALUE"];
								$PRICE = $arPrice["PRINT_VALUE"];
							else:
								$PRICE = $arPrice["PRINT_VALUE"];
							endif;
						endif;
					endforeach;
				else:
					$price_from = '';
					if ($arItem['PROPERTIES']['MAXIMUM_PRICE']['VALUE'] > $arItem['PROPERTIES']['MINIMUM_PRICE']['VALUE']) {
						$price_from = GetMessage("CR_PRICE_OT") . "&nbsp;";
					}
					CModule::IncludeModule("sale");
					$PRICE = $price_from . FormatCurrency($arItem['PROPERTIES']['MINIMUM_PRICE']['VALUE'], CSaleLang::GetLangCurrency(SITE_ID));
				endif;
			}
			
			$c = "руб.";
			if ($PRICE && strpos($PRICE, $c)) {
				$PRICE = "<span>" . str_replace($c, "</span> $c", $PRICE);
			}
			if ($DISCOUNT_PRICE && strpos($DISCOUNT_PRICE, $c)) {
				$DISCOUNT_PRICE = "<span>" . str_replace($c, "</span> $c", $DISCOUNT_PRICE);
			}
			?>
			<? $bPicture = is_array($arItem["PREVIEW_IMG"]); ?>
			<!-- <?= $key; ?>-ый рекомендуемый -->
			<div class="suggest-block" itemscope itemtype = "http://schema.org/Product" >
				<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" title="<?= $arItem["NAME"] ?>" ><?= $arItem["NAME"] ?></a> 							 
				<div class="product">
					<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" title="<?= $arItem["NAME"] ?>" >
						<img class="bm-act" src="<?= $arItem["DETAIL_PICTURE"]["SRC"] ?>" alt="<?= $arItem["NAME"] ?>"  />
					</a>
				</div>
				<div class="feature">
					<div class="price-s">
						<? if ($DISCOUNT_PRICE && $PRICE): ?>
							<div class="price-old">
								<p><?= $PRICE; ?></p>
							</div>
						<? endif; ?>
						<div class="price">
							<p><?= $DISCOUNT_PRICE ? $DISCOUNT_PRICE : $PRICE; ?></p>
						</div>
					</div>
					<div class="buy">
						<a 
							href="/catalog/<?= $arItem['IBLOCK_SECTION_ID'] ?>/<?= $arItem["ID"]; ?>/?action=ADD2BASKET&id=<?= $arItem["ID"]; ?>"
							rel="nofollow" 
							>
							<!-- onclick="return addToCart(this, 'list', 'В корзине', 'noCart');"  -->
							&nbsp;
						</a>
					</div>
				</div>
			</div>
			<!-- ///<?= $key; ?>-ый рекомендуемый -->
	<? endif; ?>
<? endforeach; ?>	
</div>
<!-- ///Рекоммендуем -->