<div class="product-content yes">
	<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
	<? if (count($arResult["ITEMS"]) > 0): ?>
		<?
		$notifyOption = COption::GetOptionString("sale", "subscribe_prod", "");
		$arNotify = unserialize($notifyOption);
		?>
		<? if ($arParams["FLAG_PROPERTY_CODE"] == "NEWPRODUCT"): ?>
			<h1 class="caption"><?= GetMessage("CR_TITLE_" . $arParams["FLAG_PROPERTY_CODE"]) ?></h1>
		<? elseif (strlen($arParams["FLAG_PROPERTY_CODE"]) > 0): ?>
			<h1 class="caption"><?= GetMessage("CR_TITLE_" . $arParams["FLAG_PROPERTY_CODE"]) ?></h1>
		<? endif ?>
		<div class="page-prod">
			<div class="sort"><a href="#">Лидеры продаж</a><a class="act" href="#">Новинки</a></div>
			<div class="switch one"><a href="#" class="prev"></a></div>
			<div class="switch two"><a href="#" class="next"></a></div>
			<div class="tables">
				<div class="products-sample">
					<?
					foreach ($arResult["ITEMS"] as $key => $arItem):
						if (is_array($arItem)) {
							$bPicture = is_array($arItem["PREVIEW_IMG"]);
							?>
							<? if ($bPicture): ?>
								<a class="link" href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><img class="product-cover" itemprop="image" src="<?= $arItem["PREVIEW_IMG"]["SRC"] ?>" width="<?= $arItem["PREVIEW_IMG"]["WIDTH"] ?>" height="<?= $arElement["PREVIEW_IMG"]["HEIGHT"] ?>" alt="<?= $arElement["NAME"] ?>" /></a></td></tr></table>
							<? else: ?>
								<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">нет картинки</a>
							<? endif ?>
							<div class="page-product-content">
								<div>
									<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" >
										Microsoft Xbox 360, 250Gb + игра "Forza 4" + игра "Ведьмак 2" + игра "Far Cry 3" 
									</a>
								</div>
								<div class="price-prod">
									<?
									var_dump($arItem["PRINT_MIN_OFFER_PRICE"]);
									if (is_array($arItem["OFFERS"]) && !empty($arItem["OFFERS"])) {	//if product has offers
										if (count($arItem["OFFERS"]) > 1) {
											?>
											<? echo GetMessage("CR_PRICE_OT"); ?>
											<? echo $arItem["PRINT_MIN_OFFER_PRICE"]; ?>
											<?
										} else {
											foreach ($arItem["OFFERS"] as $arOffer):
												?>
												<? foreach ($arOffer["PRICES"] as $code => $arPrice): ?>
													<? if ($arPrice["CAN_ACCESS"]): ?>
														<? if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]): ?>
															<p><span><?= $arPrice["PRINT_DISCOUNT_VALUE"] ?></span></p>
															<p><span><?= $arPrice["PRINT_VALUE"] ?></span></p>
														<? else: ?>
															<p><span><?= $arPrice["PRINT_VALUE"] ?></span></p>
														<? endif ?>
													<? endif; ?>
												<? endforeach; ?>
												<?
											endforeach;
										}
									}
									else { // if product doesn't have offers
										$numPrices = count($arParams["PRICE_CODE"]);
										foreach ($arItem["PRICES"] as $code => $arPrice):
											if ($arPrice["CAN_ACCESS"]):
												?>
												<? if ($numPrices > 1): ?><p style="padding: 0; margin-bottom: 5px;"><?= $arResult["PRICES"][$code]["TITLE"]; ?>:</p><? endif ?>
									<? if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]): ?>
													<p><span><?= $arPrice["PRINT_DISCOUNT_VALUE"] ?></span></p>
													<p><span><?= $arPrice["PRINT_VALUE"] ?></span></p>
												<? else: ?>
													<p><span><?= $arPrice["PRINT_VALUE"] ?></span></p>
												<?
												endif;
											endif;
										endforeach;
									}
									?>
									<?
									if (is_array($arItem["OFFERS"]) && !empty($arItem["OFFERS"])) {
										?>
										<div class="but-buy"><a href="javascript:void(0)"  onclick="return showOfferPopup(this, 'list', '<?= GetMessage("CATALOG_IN_CART") ?>', <?= CUtil::PhpToJsObject($arItem["SKU_ELEMENTS"]) ?>, <?= CUtil::PhpToJsObject($arItem["SKU_PROPERTIES"]) ?>, <?= CUtil::PhpToJsObject($arResult["POPUP_MESS"]) ?>, 'cart');"><? echo GetMessage("CATALOG_ADD") ?></a></div>
										<?
									} else {
										?>
										<? if ($arItem["CAN_BUY"]): ?>
											<div class="but-buy"><a href="<?= $arItem["ADD_URL"] ?>"  onclick="return addToCart(this, 'list', '<?= GetMessage("CATALOG_IN_CART") ?>', 'noCart');"><?= GetMessage("CATALOG_ADD") ?></a></div>
										<? endif ?>
										<?
									}
									?>
								</div>
							</div>
							<? if (!(is_array($arItem["OFFERS"]) && !empty($arItem["OFFERS"])) && !$arItem["CAN_BUY"] || is_array($arItem["OFFERS"]) && !empty($arItem["OFFERS"]) && $arItem["ALL_SKU_NOT_AVAILABLE"]):
								?>
								<div ><?= GetMessage("CATALOG_NOT_AVAILABLE2") ?></div>
							<? endif ?>
							<?
						}
					endforeach;
					?>
					<a id="prev<?= ToLower($arParams["FLAG_PROPERTY_CODE"]) ?>" class="prev" href="#">&lt;</a>
					<a id="next<?= ToLower($arParams["FLAG_PROPERTY_CODE"]) ?>" class="next" href="#">&gt;</a>
					<div id="pager<?= ToLower($arParams["FLAG_PROPERTY_CODE"]) ?>" class="pager"></div>
				<? elseif ($USER->IsAdmin()): ?>
					<h3 class="hitsale"><span></span><?= GetMessage("CR_TITLE_" . $arParams["FLAG_PROPERTY_CODE"]) ?></h3>
					<div class="listitem-carousel">
					<?= GetMessage("CR_TITLE_NULL") ?>
					</div>
				<? endif; ?>
			</div>
		</div>	
	</div>
</div>
<script type="text/javascript">
	$('#foo_<?= ToLower($arParams["FLAG_PROPERTY_CODE"]) ?>').carouFredSel({prev: '#prev<?= ToLower($arParams["FLAG_PROPERTY_CODE"]) ?>', next: '#next<?= ToLower($arParams["FLAG_PROPERTY_CODE"]) ?>', pagination: "#pager<?= ToLower($arParams["FLAG_PROPERTY_CODE"]) ?>", auto: false, height: 'auto', circular: true, infinite: false, cookie: true});
	function setEqualHeight(columns) {
		var tallestcolumn = 0;
		columns.each(function() {
			currentHeight = $(this).height();
			if (currentHeight > tallestcolumn) {
				tallestcolumn = currentHeight;
			}
		});
		columns.height(tallestcolumn);
	}
	$(document).ready(function() {
		setEqualHeight($("#foo_<?= ToLower($arParams["FLAG_PROPERTY_CODE"]) ?> .R2D2"));
		setEqualHeight($(".listitem .R2D2"));
		var countli = $(".caroufredsel_wrapper ul li").size()
		if (countli < 4) {
			$(".listitem-carousel").find(".next").addClass("disabled")
		}
	});
</script>