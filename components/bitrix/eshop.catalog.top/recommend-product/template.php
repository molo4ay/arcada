<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="similar">
	<h3>С ЭТИМ ТОВАРОМ ПОКУПАЮТ</h3>
	<? foreach ($arResult["ITEMS"] as $k => $arItem): ?>
		<? if (is_array($arItem)): ?>
			<!-- <?= $k; ?>-ый рекомендуемый -->
			<div>
				<img src="<?= $arItem["DETAIL_PICTURE"]["SRC"] ?>" alt="<?= $arItem["NAME"] ?>">
				<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a>
			</div>
			<!-- ///<?= $k; ?>-ый рекомендуемый -->
		<? endif; ?>
	<? endforeach; ?>	
</div>
<!-- ///Рекоммендуем -->