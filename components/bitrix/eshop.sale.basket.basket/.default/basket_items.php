<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?$numCells = 0;?>
<div class="clear"></div>
<? if ($arResult["ITEMS"]["AnDelCanBuy"]): ?>
	<div class="corzina">
		<form>
			<h1>КОРЗИНА</h1>
			<table id="tables-in-corz">
				<tbody id="tables-corz">
					<tr>
						<?if (in_array("NAME", $arParams["COLUMNS_LIST"])):?>
							<td>&nbsp;<?= GetMessage("SALE_NAME")?>&nbsp;</td>
							<?$numCells++;?>
						<?endif;?>
						<?if (in_array("QUANTITY", $arParams["COLUMNS_LIST"])):?>
							<td class="cart-item-quantity"><?= GetMessage("SALE_QUANTITY")?></td>
							<?$numCells++;?>
						<?endif;?>
						<?if (in_array("PRICE", $arParams["COLUMNS_LIST"])):?>
							<td class="cart-item-price"><?= GetMessage("SALE_PRICE")?></td>
							<?$numCells++;?>
							<td>&nbsp;<?= GetMessage("SALE_SUM")?>&nbsp;</td>
							<?$numCells++;?>
						<?endif;?>
						<td></td>
						<?$numCells++;?>
					</tr>
					<? $i=0; ?>
					<? foreach($arResult["ITEMS"]["AnDelCanBuy"] as $arBasketItems): ?>
						<tr>
							<?if (in_array("NAME", $arParams["COLUMNS_LIST"])):?>
								<td>
									<?if (strlen($arBasketItems["DETAIL_PAGE_URL"])):?>
										<a href="<?=$arBasketItems["DETAIL_PAGE_URL"]?>">
									<?endif;?>								
									<?=$arBasketItems["NAME"] ?>
									<?if (strlen($arBasketItems["DETAIL_PAGE_URL"])):?>
										</a>
									<?endif;?>								
								</td>
							<?endif;?>
							<?if (in_array("QUANTITY", $arParams["COLUMNS_LIST"])):?>
								<td>
									<input maxlength="18" type="text" name="QUANTITY_<?=$arBasketItems["ID"]?>" value="<?=$arBasketItems["QUANTITY"]?>" size="3" id="QUANTITY_<?=$arBasketItems["ID"]?>" class="qty" >
								</td>
							<?endif;?>
							<?if (in_array("PRICE", $arParams["COLUMNS_LIST"])):?>
								<td>
									<?if(doubleval($arBasketItems["DISCOUNT_PRICE_PERCENT"]) > 0):?>
										<div class="discount-price"><?=$arBasketItems["PRICE_FORMATED"]?></div>
										<div class="old-price"><?=$arBasketItems["FULL_PRICE_FORMATED"]?></div>
									<?else:?>
										<div class="price"><?=$arBasketItems["PRICE_FORMATED"];?></div>
									<?endif?>							
								</td>
								<td><?= $arBasketItems["PRICE"] * $arBasketItems["QUANTITY"]?> руб.</td>
							<?endif;?>
							<td>
								<a href="<?=str_replace("#ID#", $arBasketItems["ID"], $arUrlTempl["delete"])?>"  title="<?=GetMessage("SALE_DELETE_PRD")?>"></a>
							</td>
						</tr>
						<? $i++; ?>
					<? endforeach; ?>				
					<tr>
						<td colspan="<?= $numCells; ?>" >ОБЩАЯ СУММА: <span><?=$arResult["allSum_FORMATED"]?></span></td>
					</tr>
				</tbody>
			</table>
			<?if ($arParams["HIDE_COUPON"] != "Y"):?>
				<div class="tab-but">
					<div>
						<p>У вас есть дисконтная карта для получения скидки?</p>
						<p>Введите код с карты:</p>
					</div>
					<div class="tab-but">
						<input id="text-tabl"
						<?if(empty($arResult["COUPON"])):?>
							onclick="if (this.value=='<?=GetMessage("SALE_COUPON_VAL")?>')this.value=''; this.style.color='black'"
							onblur="if (this.value=='') {this.value='<?=GetMessage("SALE_COUPON_VAL")?>'; this.style.color='#a9a9a9'}"
							style="color:#a9a9a9"
						<?endif;?>
							value="<?if(!empty($arResult["COUPON"])):?><?=$arResult["COUPON"]?><?else:?><?=GetMessage("SALE_COUPON_VAL")?><?endif;?>"
							name="COUPON">					

						<? if ($arResult["DISCOUNT_PRICE_ALL"]): ?>
							<p>Ваша скидка<span><?= $arResult["DISCOUNT_PRICE_ALL_FORMATED"]; ?></span></p>
						<? endif; ?>

						<input id="button-tabl" type="image" src="<?= SITE_TEMPLATE_PATH ?>/images/symma-pas.png" value="<?echo GetMessage("SALE_UPDATE")?>" name="BasketRefresh" />
					</div>
				</div>
			<?endif;?>		

			<div class="last-corz">
				<input id="button-tabl2" src="<?= SITE_TEMPLATE_PATH ?>/images/zacaz-tabl-pas.png" type="image" value="<?echo GetMessage("SALE_ORDER")?>" name="BasketOrder" >
			</div>
		</form>
	</div>
<? else: ?>
	<h1 class="hmessage"><?= GetMessage("SALE_NO_ACTIVE_PRD"); ?></h1>
<? endif; ?>
