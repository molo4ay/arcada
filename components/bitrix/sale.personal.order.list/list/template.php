<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>



<!-- ////Поисковик и корзина -->
<div class="clear"></div>
<div class="order">
	<h1>ИСТОРИЯ ЗАКАЗОВ</h1>


	<?
	if ($_REQUEST["filter_canceled"] == "Y" && $_REQUEST["filter_history"] == "Y")
		$page = "canceled";
	elseif ($_REQUEST["filter_status"] == "F" && $_REQUEST["filter_history"] == "Y")
		$page = "completed";
	elseif ($_REQUEST["filter_history"] == "Y")
		$page = "all";
	else
		$page = "active";
	?>

	<div class="sort tabfilter">
		<div class="sorttext"><?= GetMessage("STPOL_F_NAME") ?></div>
		<a class="sortbutton active" href="<? if ($page != "active")
		echo $arResult["CURRENT_PAGE"] . "?filter_history=N";
	else
		echo "javascript:void(0)";
	?>"><?= GetMessage("STPOL_F_ACTIVE") ?></a>

		<a class="sortbutton all" href="<? if ($page != "all")
				 echo $arResult["CURRENT_PAGE"] . "?filter_history=Y";
			 else
				 echo "javascript:void(0)";
	?>"><?= GetMessage("STPOL_F_ALL") ?></a>

		<a class="sortbutton completed" href="<? if ($page != "completed")
				 echo $arResult["CURRENT_PAGE"] . "?filter_status=F&filter_history=Y";
			 else
				 echo "javascript:void(0)";
	?>"><?= GetMessage("STPOL_F_COMPLETED") ?></a>

		<a class="sortbutton canceled" href="<? if ($page != "canceled")
				 echo $arResult["CURRENT_PAGE"] . "?filter_canceled=Y&filter_history=Y";
			 else
				 echo "javascript:void(0)";
	?>"><?= GetMessage("STPOL_F_CANCELED") ?></a>

	</div>
<div id="sb-filter">
	<div class="filtr-sort">
		<a href="/personal/ " class="sortirovka">Личный кабинет</a>
		<a href="/personal/order/" class="sortirovka actsort">Мои заказы</a>
	</div>
</div>

	<table>
		<tr>
			<td>Номер заказа</td>
			<td>Дата заказа</td>
			<td>Сумма заказа</td>
			<td></td>
		</tr>
		<?
		$bNoOrder = true;
		foreach ($arResult["ORDERS"] as $key => $val) {
			$bNoOrder = false;
			?>
			<tr>
				<td>
					<a href="#">№<?= $val["ORDER"]["ACCOUNT_NUMBER"] ?></a>
				</td>
				<td><p><?= $val["ORDER"]["DATE_INSERT"]; ?></p></td>
				<td><?= $val["ORDER"]["FORMATED_PRICE"] ?></td>								
				<td><a href="<?= $val["ORDER"]["URL_TO_DETAIL"] ?>">Показать детали заказа</a></td>
			</tr>
			<?
			if (IntVal($val["ORDER"]["DELIVERY_ID"]) > 0) {
				echo "<strong>" . GetMessage("P_DELIVERY") . "</strong> " . $arResult["INFO"]["DELIVERY"][$val["ORDER"]["DELIVERY_ID"]]["NAME"] . "<br><br>";
			} elseif (strpos($val["ORDER"]["DELIVERY_ID"], ":") !== false) {
				echo "<strong>" . GetMessage("P_DELIVERY") . "</strong> ";
				$arId = explode(":", $val["ORDER"]["DELIVERY_ID"]);
				echo $arResult["INFO"]["DELIVERY_HANDLERS"][$arId[0]]["NAME"] . " (" . $arResult["INFO"]["DELIVERY_HANDLERS"][$arId[0]]["PROFILES"][$arId[1]]["TITLE"] . ")" . "<br><br>";
			}

			if (strlen($val["ORDER"]["TRACKING_NUMBER"]) > 0) {
				echo "<strong>" . GetMessage("P_ORDER_TRACKING_NUMBER") . "</strong> ";
				echo $val["ORDER"]["TRACKING_NUMBER"] . "<br/><br/>";
			}
		}
		?>
	</table>
</div>		
<?
if ($bNoOrder) {
	echo ShowNote(GetMessage("STPOL_NO_ORDERS_NEW"));
}
?>
<? if (strlen($arResult["NAV_STRING"]) > 0): ?>
	<div class="navigation"><?= $arResult["NAV_STRING"] ?></p>
	<? endif
?>






