<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="clear"></div>
<div class="tovar">
	<div class="suggest-block tovs">
		<div class="product tovars"><img class="bm-act" src="<?= SITE_TEMPLATE_PATH ?>/images/covers.jpg" alt="covers.jpg"></div>
		<div class="feature vn-tovar">
			<div id="status">
				<p>Статус:</p>
				<a href="#">В наличии</a>
			</div>
			<div class="price-s">
				<div><p><span>18 300</span> руб.</p></div>
				<div class="price"><p><span>14 600</span> руб.</p></div>
			</div>
			<div class="buy">
				<a href="#"></a>
			</div>
		</div>
	</div>
	<div id="about">
		<div class="stars">
			<img src="<?= SITE_TEMPLATE_PATH ?>/images/star-pas.png" alt="star-pas.png">
			<img src="<?= SITE_TEMPLATE_PATH ?>/images/star-act.png" alt="star-act.png">
			<img src="<?= SITE_TEMPLATE_PATH ?>/images/star-act.png" alt="star-act.png">
			<img src="<?= SITE_TEMPLATE_PATH ?>/images/star-act.png" alt="star-act.png">
			<img src="<?= SITE_TEMPLATE_PATH ?>/images/star-act.png" alt="star-act.png">
		</div>
		<p>Жанр: </p><a href="#">Экшен</a><br>
		<p>Для чего: </p><a href="#">PC</a><br>
		<p class="lasts" >Серия: </p><a href="#">Call of Juarez</a><br>
		<p class="lasts2" >Игра Call of Juarez. Gunslinger - это очередная глава в знаменитой серии вестернов Call of Juarez.232323</p><br>
		<p>Выход: </p><a href="#">14.06.2013</a><br>
		<p>Язык: </p><a href="#">Английский (с русскими субтитрами)</a><br>
		<p>Разработчик: </p><a href="#">Techland</a><br>
		<p>Издатель: </p><a href="#">Ubisoft</a><br>
		<p>Издатель / Дистрибьютор в России: </p><a href="#">1С-СофтКлаб</a><br>
		<p>Носитель: </p><a href="#">DVD</a><br>
		<p>Упаковка: </p><a href="#">Jewel</a><br>
		<p>Артикул: </p><span >1CSC20000561</span><br>
		<p>Количество игроков: </p><a href="#">Один игрок</a><br>
	</div>
	<div class="scrin">
		<h1>СКРИНШОТЫ</h1>
		<div>
			<img src="<?= SITE_TEMPLATE_PATH ?>/images/scrin-2-2.jpg" alt="scrin-2-2.jpg">
			<img src="<?= SITE_TEMPLATE_PATH ?>/images/scrin-2-1.jpg" alt="scrin-2-1.jpg">
			<img src="<?= SITE_TEMPLATE_PATH ?>/images/scrin-2-2.jpg" alt="scrin-2-2.jpg">
			<img src="<?= SITE_TEMPLATE_PATH ?>/images/scrin-2-1.jpg" alt="scrin-2-1.jpg">
		</div>
	</div>
	<div class="similar">
		<h1>С ЭТИМ ТОВАРОМ ПОКУПАЮТ</h1>
		<div>
			<img src="<?= SITE_TEMPLATE_PATH ?>/images/oblogka-1.jpg" alt="oblogka-1.jpg">
			<a href="#">Red Dead Redemption</a>
		</div>
		<div>
			<img src="<?= SITE_TEMPLATE_PATH ?>/images/oblogka-2.jpg" alt="oblogka-2.jpg">
			<a href="#">Сайлент Хилл 3</a>
		</div>
		<div>
			<img src="<?= SITE_TEMPLATE_PATH ?>/images/oblogka-3.jpg" alt="oblogka-3.jpg">
			<a href="#">The Baconing</a>
		</div>
		<div>
			<img src="<?= SITE_TEMPLATE_PATH ?>/images/oblogka-4.jpg" alt="oblogka-4.jpg">
			<a href="#">Dead Space 3</a>
		</div>
		<div>
			<img src="<?= SITE_TEMPLATE_PATH ?>/images/oblogka-5.jpg" alt="oblogka-5.jpg">
			<a href="#">Guitar Hero World Tour</a>
		</div>
	</div>
</div>
<div class="abouts">
	<div>
		<h1>ОПИСАНИЕ</h1>
		<p><span>Call of Juarez. Gunslinger:</span></p>
		<p>В игре Call of Juarez. Gunslinger есть все, что должно быть в рассказе о Диком Западе – от золотых приисков до грязных салунов.</p>
		<p>Отправляйтесь по следам самых опасных преступников в роли отчаянного охотника за головами. Переступите границу, за которой человек становится легендой, и узнайте правду о тех, чьи имена увековечены в бесчисленных историях о Диком Западе.</p>
	</div>
	<div>
		<h1>ОСОБЕННОСТИ</h1>
		<p>Билли Кид, Пэт Гарретт, Джесси Джеймс – эти люди стали легендами Дикого Запада, и теперь вам выпадет возможность встретиться с ними лицом к лицу.</p>
		<p>В этих землях нет закона: да не введут вас в заблуждение потрясающие пейзажи – в бескрайних пустошах опасности подстерегают на каждом шагу.</p>
		<p>На Диком Западе прав тот, у кого пистолет. А настоящий охотник за головами большую часть времени проводит в перестрелках.</p>
		<p>Стреляйте из пистолетов и винтовок, уворачивайтесь от пуль и уничтожайте нескольких противников сразу.</p>
		<p>Выберите уникальную оружейную специализацию – развивайтесь и приобретайте новые навыки, чтобы стать лучшим стрелком Дикого Запада.</p>
	</div>
	<div>
		<h1>СИСТЕМНЫЕ ТРЕБОВАНИЯ</h1>
		<p><span>Минимальные cистемные требования:</span></p>
		<p>Операционная система Windows Vista/XP, Windows 7, Windows 8;
			Процессор Intel Core 2 Duo с тактовой частотой 2 ГГц/AMD Athlon X2 с тактовой частотой 2 ГГц;
			2 Гб оперативной памяти;
			9 Гб свободного места на жестком диске;
			Видеокарта NVIDIA Geforce 8800GTS/ATI Radeon HD 3870 или лучше, совместимая с DirectX 9.0c;
			Звуковая карта совместимая с DirectX 9.0c;
			Устройство для чтения DVD;
			Клавиатура и мышь;
			Для активации игры требуется интернет-соединение, а также учетная запись Steam.
			Рекомендуемые cистемные требования:</p>
		<p>Операционная система Windows Vista, Windows 7, Windows 8;<br>
			Процессор Intel Core 2 Duo с тактовой частотой 3 ГГц/AMD Athlon X2 с тактовой частотой 3 ГГц;<br>
			4 Гб оперативной памяти;<br>
			9 Гб свободного места на жестком диске;<br>
			Видеокарта NVIDIA GeForce 280 GTX/ATI Radeon HD 4890 или лучше, совместимая с DirectX 9.0c;<br>
			Звуковая карта совместимая с DirectX 9.0c;<br>
			Устройство для чтения DVD;<br>
			Клавиатура и мышь;<br>
			Для активации игры требуется интернет-соединение, а также учетная запись Steam.</p>
	</div>
</div>