<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $PRICE = $arResult["PRICES"][$arParams["PRICE_CODE"][0]]; ?>
<? $PIC_SRC = is_array($arResult["DETAIL_PICTURE"]) ? $arResult["DETAIL_PICTURE"]["SRC"] : "/images/cover.png"; ?>
<!-- ////Поисковик и корзина -->
<div class="clear"></div>
<!-- pre><? print_r($arParams); ?></pre>
<pre><? print_r($arResult); ?></pre -->
<div class="tovar">
	<div class="suggest-block tovs">
		<div class="product tovars">
			<img class="bm-act" src="<?= $PIC_SRC ?>" alt="<?= $arResult['NAME']; ?>">
		</div>
		<div class="feature vn-tovar">
			<div id="status">
				<p>Статус:</p>
				<a href="#"><?= $arResult["PROPERTIES"]["STATUS_TOVARA"]["VALUE"] ? $arResult["PROPERTIES"]["STATUS_TOVARA"]["VALUE"] : "-"; ?></a>
			</div>
			<div class="price-s">
				<? if ($PRICE["DISCOUNT_VALUE"] != $PRICE["VALUE"]): ?>
					<div class="price-old">
						<p><span><?=$PRICE["VALUE"]?></span> руб.</p>
					</div>
				<? endif; ?>
				<div class="price">
					<p><span><?=$PRICE["DISCOUNT_VALUE"]?></span> руб.</p>
				</div>
			</div>
			<div class="buy">
				<a href="<?echo $arResult["ADD_URL"]?>"></a>
			</div>
		</div>
	</div>
	<div id="about">
		<div class="stars">
			<?$APPLICATION->IncludeComponent(
				"altasib:review.rating",
				"",
				Array(
					"IBLOCK_TYPE" => "catalog",
					"IBLOCK_ID" => $arResult["IBLOCK_ID"],
					"IS_SEF" => "N",
					"ELEMENT_ID" => $arResult["ID"],
					"ELEMENT_CODE" => $arResult["CODE"],
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "86400",
					"CACHE_NOTES" => "",
					"SHOW_TITLE" => "N",
					"TITLE_TEXT" => "Рейтинг товара:"
				),
			false
			);?>			
		</div>
		<? if (is_array($arResult['DISPLAY_PROPERTIES']) && count($arResult['DISPLAY_PROPERTIES']) > 0): ?>
			<? $arPropertyRecommend = $arResult["DISPLAY_PROPERTIES"]["RECOMMEND"]; ?>
			<? unset($arResult["DISPLAY_PROPERTIES"]["RECOMMEND"]); ?>
			<? if (is_array($arResult['DISPLAY_PROPERTIES']) && count($arResult['DISPLAY_PROPERTIES']) > 0):?>
				<?foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
					<p><?=$arProperty["NAME"]?>:</p>
					<b>
						<? if(is_array($arProperty["DISPLAY_VALUE"])):
							echo implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);
						elseif($pid=="MANUAL"):
							?><a href="<?=$arProperty["VALUE"]?>"><?=GetMessage("CATALOG_DOWNLOAD")?></a><?
						else:
							echo $arProperty["DISPLAY_VALUE"];?>
						<?endif?>
					</b><br />
				<?endforeach?>
			<?endif?>
		<? endif; ?>
		<!-- p>Для чего: </p><a href="#">PC</a><br -->
		<!-- p>Серия: </p><a href="#">Call of Juarez</a><br -->
		<!-- p>Игра Call of Juarez. Gunslinger - это очередная глава в знаменитой серии вестернов Call of Juarez.</p><br -->
		<!-- +++ p>Выход: </p><a href="#">14.06.2013</a><br -->
		<!-- +++ p>Язык: </p><a href="#">Английский (с русскими субтитрами)</a><br -->
		<!-- +++ p>Разработчик: </p><a href="#">Techland</a><br -->
		<!-- +++ p>Издатель: </p><a href="#">Ubisoft</a><br -->
		<!-- p>Издатель / Дистрибьютор в России: </p><a href="#">1С-СофтКлаб</a><br -->
		<!-- p>Носитель: </p><a href="#">DVD</a><br -->
		<!-- p>Упаковка: </p><a href="#">Jewel</a><br -->
		<!-- +++ p>Артикул: </p><span >1CSC20000561</span><br -->
		<!-- +++ p>Количество игроков: </p><a href="#">Один игрок</a><br -->
	</div>
	
	<div class="clear" style="height: 5px;"></div>
	
	<? if (count($arResult['MORE_PHOTO'])): ?>
		<script type="text/javascript">
		$(document).ready(function() {
			$('.catalog-detail-images').fancybox({
				'transitionIn': 'elastic',
				'transitionOut': 'elastic',
				'speedIn': 600,
				'speedOut': 200,
				'overlayShow': false,
				'cyclic' : true,
				'padding': 20,
				'titlePosition': 'over',
				'type' : 'image',
				'onComplete': function() {
				$("#fancybox-title").css({ 'top': '100%', 'bottom': 'auto' });
				}
			});
		});
		</script>	
		<div class="scrin">
			<h3>СКРИНШОТЫ</h3>
			<div style="position:relative; text-align: center;">
					<ul>
						<?foreach($arResult["MORE_PHOTO"] as $key => $PHOTO):?>
							<? if ($key > 3) break; ?>
							<li><a rel="catalog-detail-images" class="catalog-detail-images" href="<?=$PHOTO['SRC']?>" title="<?=htmlspecialcharsbx(strlen($PHOTO["DESCRIPTION"]) > 0 ? $PHOTO["DESCRIPTION"] : $PHOTO["NAME"])?>"><img  src="<?=$PHOTO["SRC"]?>"  alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>" /></a></li>
						<?endforeach?>
					</ul>
			</div>
		</div>
	<? endif; ?>
	<!-- recommend -->
	<?if(isset($arPropertyRecommend) && count($arPropertyRecommend["DISPLAY_VALUE"]) > 0):?>
		<?
		global $arRecPrFilter;
		$arRecPrFilter["ID"] = $arPropertyRecommend["VALUE"];
		$APPLICATION->IncludeComponent("bitrix:eshop.catalog.top", "recommend-product", array(
				"IBLOCK_TYPE" => "",
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"ELEMENT_SORT_FIELD" => "sort",
				"ELEMENT_SORT_ORDER" => "desc",
				"ELEMENT_COUNT" => $arParams["ELEMENT_COUNT"],
				//"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
				"BASKET_URL" => $arParams["BASKET_URL"],
				"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
				"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
				"CACHE_TYPE" => $arParams["CACHE_TYPE"],
				"CACHE_TIME" => $arParams["CACHE_TIME"],
				"DISPLAY_COMPARE" => "N",
				"PRICE_CODE" => $arParams["PRICE_CODE"],
				"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
				"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
				"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
				"FILTER_NAME" => "arRecPrFilter",
				"DISPLAY_IMG_WIDTH"	 =>	$arParams["DISPLAY_IMG_WIDTH"],
				"DISPLAY_IMG_HEIGHT" =>	$arParams["DISPLAY_IMG_HEIGHT"],
				"SHARPEN" => $arParams["SHARPEN"],
				"ELEMENT_COUNT" => 30,
				"CONVERT_CURRENCY" => $arParams['CONVERT_CURRENCY'],
				"CURRENCY_ID" => $arParams['CURRENCY_ID'],
			),
			false,
			Array('HIDE_ICONS' => 'Y')
		);
		?>
	<?endif;?>		
</div>

<div class="clear" style="height: 30px;"></div>

<!-- ТОВАР -->
<div class="abouts">
	<?echo $arResult["DETAIL_TEXT"];?>
</div>
<!-- ///ТОВАР -->

<!-- Обзор и Видео -->
<? if (isset($arResult['REVIEWS'])): ?>
<div class="abouts">
	<div>
		<h1>ОБЗОРЫ</h1>
		<? foreach ($arResult['REVIEWS'] as $REVIEW): ?>
		<? if ($REVIEW['VIDEO_YOUTUBE']['VALUE']): ?>
			<div class="video-obzor">
				<? foreach ($REVIEW['VIDEO_YOUTUBE']['VALUE'] as $VIDEO): ?>
					<iframe width="640" height="390" src="//www.youtube.com/embed/<?= substr($VIDEO, strpos($VIDEO, 'v=') + 2); ?>" frameborder="0" allowfullscreen></iframe>
					<br /><br />
				<? endforeach; ?>
			</div>
		<? endif; ?>
		<div>
			<?= $REVIEW['DETAIL_TEXT'] ?>
		</div>
		<? endforeach; ?>
	</div>
</div>
<? else: ?>
<script type="text/javascript">
	$(function() {
		var $s = $('.sortirovka');
		$s.eq(1).remove();
	});	
</script>
<? endif; ?>
<!-- ///Обзор и Видео -->

<!-- Отзывы -->
<div class="abouts">
	<div>
		<?$APPLICATION->IncludeComponent(
			"altasib:review",
			"",
			Array(
				"AJAX_MODE" => "N",
				"IBLOCK_TYPE" => "catalog",
				"IBLOCK_ID" => $arResult["IBLOCK_ID"],
				"IS_SEF" => "N",
				"ELEMENT_ID" => $arResult["ID"],
				"ELEMENT_CODE" => $arResult["CODE"],
				"USE_CAPTCHA" => "Y",
				"ALLOW_VOTE" => "Y",
				"MESSAGE_OK" => "Отзыв добавлен",
				"POST_DATE_FORMAT" => "j M Y",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "3600",
				"ALLOW_TITLE" => "N",
				"ALLOW_ENTER_SITE" => "N",
				"ADD_TITLE" => "",
				"LIST_TITLE" => "",
				"ONLY_AUTH_COMPLAINT" => "Y",
				"MOD_GOUPS" => array(),
				"SEND_TO_MOD_IF_HAVE_LINK" => "N",
				"ONLY_AUTH_SEND" => "N",
				"SAVE_RATING" => "N",
				"EMPTY_MESSAGE" => "",
				"COMMENTS_MODE" => "N",
				"NOT_HIDE_FORM" => "N",
				"REVIEWS_ON_PAGE" => "20",
				"PAGE_NAVIGATION_TEMPLATE" => "",
				"PAGE_NAVIGATION_SHOW_ALL" => "N",
				"PAGE_NAVIGATION_SHOW_ALWAYS" => "N",
				"ALLOW_SUBS" => "Y",
				"ALLOW_SUBS_AUTH_ONLY" => "Y",
				"REQUIRED_RATING" => "Y",
				"PLUS_TEXT_MIN_LENGTH" => "5",
				"PLUS_TEXT_MAX_LENGTH" => "",
				"MINUS_TEXT_MIN_LENGTH" => "",
				"MINUS_TEXT_MAX_LENGTH" => "",
				"MIN_LENGTH" => "5",
				"MAX_LENGTH" => "",
				"NAME_SHOW_TYPE" => "LOGIN",
				"SHOW_AVATAR_TYPE" => "ns",
				"AVATAR_WIDTH" => "80",
				"AVATAR_HEIGHT" => "80",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N"
			),
		false
		);?>		
		<!--
		<h1>Отзывы</h1>
		<form id="otziv">
			<div class="ab-in">
				<span>Имя:</span><br>
				<input id="name-oziv" type="text" name="" size="15" maxlength="30">
			</div>
			<div class="ab-in">
				<span>Введите капчу:</span><br>
				<input id="capch-oziv" type="text" name="" size="15" maxlength="30">
				<img id="test-oziv" src="images/test-otz.jpg" alt="test-otz">
			</div>
			<div class="comment-oz">
				<p>осталось<span>1000</span>знаков</p>
				<span>Ваш комментарий:</span><br>
				<textarea maxlength="1000" placeholder="Ваше сообщение">
				</textarea>
				<div>
				<input class="zv-send otz-otp" src="images/zvonok-but-pas.png" type="image" alt="OK">
				</div>
			</div>
		</form>
		<div id="commentarii">
			<h2>Екатерина | 20/02/2013 | 20:40:36</h2>
			<textarea disabled>Прошла 3 раза! Супер однозначно!</textarea>
			<h2>killkill11 | 20/02/2013 | 20:40:36</h2>
			<textarea disabled>Мне игра очень понравилась! Всем советую!</textarea>
			<h2>killkill11 | 20/02/2013 | 20:40:36</h2>
			<textarea disabled>Мне игра очень понравилась! Всем советую!</textarea>
			<h2>killkill11 | 20/02/2013 | 20:40:36</h2>
			<textarea disabled>Мне игра очень понравилась! Всем советую!</textarea>
		</div>
	-->
	</div>
</div>
<!-- ///Отзывы -->