<!-- Сортировка -->
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>	

<? if (isset($_REQUEST['f']) && $_REQUEST['f'] === 'list'): ?>
	<!-- Рекоммендуем -->
	<div class="pred-tabl"></div>
	<table id="tables-in">
		<tbody id="tables">
			<tr>
				<td>НАЗВАНИЕ</td>
				<td>СТАТУС</td>
				<td>ЦЕНА</td>
				<td></td>
			</tr>
			<? foreach($arResult["ITEMS"] as $cell=>$arElement): ?>
				<? $PRICE = $arElement["PRICES"][$arParams["PRICE_CODE"][0]]; ?>
				<!-- <?=$cell?>-ая строка -->
				<tr>
					<td>
						<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a>
						<? if ($arElement["PROPERTIES"]["ZHANR"]["VALUE"]): ?>
							<br />
							<? $zfirst = true; ?>
							<? foreach ($arElement["PROPERTIES"]["ZHANR"]["VALUE"] as $z): ?>
								<? if (!$zfirst): ?>/<? endif; ?>
								<a href="/search/?<?= http_build_query(array('q' => $z)); ?>" class="zhanr"><?= $z ?></a>
								<? $zfirst = false; ?>
							<? endforeach; ?>
						<? endif ?>
					</td>
					<td><?= $arElement["PROPERTIES"]["STATUS_TOVARA"]["VALUE"] ? $arElement["PROPERTIES"]["STATUS_TOVARA"]["VALUE"] : "-"; ?></td>
					<td>
							<? if ($PRICE["DISCOUNT_VALUE"] != $PRICE["VALUE"]): ?>
								<p class="old-price"><?=$PRICE["VALUE"]?> руб.</p>
							<? endif; ?>
							<p class="price"><?=$PRICE["DISCOUNT_VALUE"]?> руб.</p>
					</td>
					<td>
						<? if ($arElement["CAN_BUY"]): ?>
							<div class="but-buy">
								<a href="<?=$arElement["ADD_URL"]?>"></a>
							</div>
						<? endif; ?>
					</td>
				</tr>
				<!-- ///<?=$cell?>-ый блок -->
			<? endforeach; ?>
		</tbody>
	</table>
<? else: ?>
	<div class="suggest">
		<? foreach($arResult["ITEMS"] as $cell=>$arElement): ?>
			<? $PRICE = $arElement["PRICES"][$arParams["PRICE_CODE"][0]]; ?>
			<? $PIC_SRC = is_array($arElement["DETAIL_PICTURE"]) ? $arElement["DETAIL_PICTURE"]["SRC"] : "/images/cover.png"; ?>
			<? if ($cell && $cell % 3 === 0): ?>
				</div><div class="suggest">
			<? endif; ?>
			<!-- <?=$cell?>-ый блок -->
			<div class="suggest-block">
				<a href="<?=$arElement["DETAIL_PAGE_URL"]?>">
					<?=$arElement["NAME"]?>
				</a>
				<div class="product">
					<a href="<?=$arElement["DETAIL_PAGE_URL"]?>" title="<?= $arElement["NAME"] ?>" >
						<img class="bm-act" src="<?=$PIC_SRC?>" alt="<?=$arElement["NAME"]?>">
					</a>
				</div>
				<div class="feature">
					<div class="price-s">
						<? if ($PRICE["DISCOUNT_VALUE"] != $PRICE["VALUE"]): ?>
							<div class="price-old">
								<p><span><?=$PRICE["VALUE"]?></span> руб.</p>
							</div>
						<? endif; ?>
						<div class="price">
							<p><span><?=$PRICE["DISCOUNT_VALUE"]?></span> руб.</p>
						</div>
					</div>
					<? if ($arElement["CAN_BUY"]): ?>
						<div class="buy">
							<a href="<?=$arElement["ADD_URL"]?>"></a>
						</div>
					<? endif; ?>
				</div>
			</div>
			<!-- ///<?=$cell?>-ый блок -->
		<? endforeach; ?>
	</div>
<?endif;?>
	
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>
<!-- ////Сортировка -->