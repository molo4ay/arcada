<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

if (empty($arResult))
	return;

$lastSelectedItem = null;
$lastSelectedIndex = -1;

foreach ($arResult as $itemIdex => $arItem) {
	if (!$arItem["SELECTED"])
		continue;

	if ($lastSelectedItem == null || strlen($arItem["LINK"]) >= strlen($lastSelectedItem["LINK"])) {
		$lastSelectedItem = $arItem;
		$lastSelectedIndex = $itemIdex;
	}
}
?>
<ul>
	<li <? if ($lastSelectedIndex < 0): ?>class="active-tm"<? endif; ?> ><a href="<?= SITE_DIR ?>" class="top-menus">Главная</a></li>
	<? foreach ($arResult as $itemIdex => $arItem): ?>
		<li <? if ($itemIdex == $lastSelectedIndex): ?>class="active-tm"<? endif; ?> ><a href="<?= $arItem["LINK"] ?>" class="top-menus"><?= $arItem["TEXT"] ?></a></li>
	<? endforeach; ?>
</ul>