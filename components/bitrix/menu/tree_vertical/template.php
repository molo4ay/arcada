<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
if (count($arResult) < 1)
return;

//echo "<pre>";
//print_r($arResult);
//echo "</pre>";

$previousLevel = 0;
?>
<? foreach ($arResult as $key => $arItem): ?>
<? if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel): ?>
<?= str_repeat("</div>", ($previousLevel - $arItem["DEPTH_LEVEL"])); ?>
<? if ($arItem['DEPTH_LEVEL'] == 1) : ?>
</div>
<? endif; ?>
<? endif; ?>
<?
$bHasSelected = $arItem['SELECTED'];

if ($arItem["IS_PARENT"]) {
$i = $key;
$childSelected = false;
if (!$bHasSelected) {	//if parent is selected, check children
while ($arResult[++$i]['DEPTH_LEVEL'] > $arItem['DEPTH_LEVEL']) {
if ($arResult[$i]['SELECTED']) {
$bHasSelected = $childSelected = true;
break; // if child is selected, select parent
}
}
}
}
?>
<? if ($arItem['DEPTH_LEVEL'] == 2 && $bHasSelected): ?>
<script type="text/javascript">
    $(function() {
        $("#left-menu-item-<?= "0" . $key; ?>").click();
    });
</script>
<? endif; ?>
<? if ($arItem['DEPTH_LEVEL'] == 1) : ?>


<div class="left-panel">
    <div class="left-panel-top">
        <span><?= $arItem["TEXT"] ?></span>
    </div>
    <div class="left-panel-bottom accordion">
        <? elseif ($arItem['DEPTH_LEVEL'] == 2): ?>
        <h3 id="left-menu-item-<?= "0" . $key; ?>"><?= $arItem["TEXT"] ?></h3>
        <div class="lfmenu">
            <? if (!$arItem["IS_PARENT"]): ?>
            <a href="<?= $arItem["LINK"] ?>" <? if ($bHasSelected): ?>class="active"<? endif; ?> ><span><?= $arItem["TEXT"] ?></span></a>
            <br />
        </div>
        <? endif; ?>
        <? elseif ($arItem['DEPTH_LEVEL'] == 3): ?>
        <a href="<?= $arItem["LINK"] ?>" <? if ($bHasSelected): ?>class="active"<? endif; ?> ><span><?= $arItem["TEXT"] ?></span></a>
        <br />
        <? endif; ?>
        <? $previousLevel = $arItem["DEPTH_LEVEL"]; ?>
        <? endforeach; ?>
    </div>
</div>
</div>






<div class="left-panel left_panel_style1">

    <div class="left-panel-bottom accordion">
        <h3 id="left-menu-item-01" class=""><table class=lm_img_wrap><tr><td><img src="/bitrix/templates/arcada_artem/images/xbox_360.png" alt="xbox_360"></td></tr></table><span><b>XBOX 360</b></span></h3>
        <div class="lfmenu">
            <a href="/catalog/21/"><img src="/bitrix/templates/arcada_artem/images/cursor_act.png" alt="cursor_act"><span>Игры</span><span class="lfmenu_r">50</span></a>
            <a href="/catalog/22/"><img src="/bitrix/templates/arcada_artem/images/cursor_act.png" alt="cursor_act"><span>Приставки</span><span class="lfmenu_r">50</span></a>
            <a href="/catalog/24/"><img src="/bitrix/templates/arcada_artem/images/cursor_act.png" alt="cursor_act"><span>Аксессуары</span><span class="lfmenu_r">50</span></a>
        </div>		
        <h3 id="left-menu-item-05" class=""><table class=lm_img_wrap><tr><td><img src="/bitrix/templates/arcada_artem/images/xbox_one.png" alt="xbox_one"></td></tr></table><span><b>XBOX One</b></span></h3>
        <div class="lfmenu">
            <a href="/catalog/34/"><img src="/bitrix/templates/arcada_artem/images/cursor_act.png" alt="cursor_act"><span>Игры</span><span class="lfmenu_r">50</span></a>
            <a href="/catalog/29/"><img src="/bitrix/templates/arcada_artem/images/cursor_act.png" alt="cursor_act"><span>Приставки</span><span class="lfmenu_r">50</span></a>
        </div>	
        <h3 id="left-menu-item-09" class=""><table class=lm_img_wrap><tr><td><img src="/bitrix/templates/arcada_artem/images/play_station.png" alt="play_station"></td></tr></table><span><b>Play Station</b></span></h3>
        <div class="lfmenu">
            <div class="left-panel-bottom accordion">
                <h3 id="left-menu-item-09" class=""><table class=lm_img_wrap><tr><td><img src="/bitrix/templates/arcada_artem/images/play_station2.png" alt="play_station2"></td></tr></table><span><b>Play Station 2</b></span></h3>
                <div class="lfmenu">
                    <a href="/catalog/21/"><img src="/bitrix/templates/arcada_artem/images/cursor_act.png" alt="cursor_act"><span>Игры</span><span class="lfmenu_r">50</span></a>
                    <a href="/catalog/22/"><img src="/bitrix/templates/arcada_artem/images/cursor_act.png" alt="cursor_act"><span>Приставки</span><span class="lfmenu_r">50</span></a>
                </div>										
                <h3 id="left-menu-item-09" class=""><table class=lm_img_wrap><tr><td><img src="/bitrix/templates/arcada_artem/images/play_station3.png" alt="play_station3"></td></tr></table><span><b>Play Station 3</b></span></h3>
                <div class="lfmenu">
                    <a href="/catalog/34/"><img src="/bitrix/templates/arcada_artem/images/cursor_act.png" alt="cursor_act"><span>Игры</span><span class="lfmenu_r">50</span></a>
                    <a href="/catalog/29/"><img src="/bitrix/templates/arcada_artem/images/cursor_act.png" alt="cursor_act"><span>Приставки</span><span class="lfmenu_r">50</span></a>
                </div>										
                <h3 id="left-menu-item-09" class=""><table class=lm_img_wrap><tr><td><img src="/bitrix/templates/arcada_artem/images/play_station4.png" alt="play_station4"></td></tr></table><span><b>Play Station 4</b></span></h3>
                <div class="lfmenu">
                    <a href="/catalog/21/"><img src="/bitrix/templates/arcada_artem/images/cursor_act.png" alt="cursor_act"><span>Игры</span><span class="lfmenu_r">50</span></a>
                    <a href="/catalog/22/"><img src="/bitrix/templates/arcada_artem/images/cursor_act.png" alt="cursor_act"><span>Приставки</span><span class="lfmenu_r">50</span></a>
                </div>	
                <h3 id="left-menu-item-09" class=""><table class=lm_img_wrap><tr><td><img src="/bitrix/templates/arcada_artem/images/psp.png" alt="psp"></td></tr></table><span><b>PSP</b></span></h3>
                <div class="lfmenu">
                    <a href="/catalog/21/"><img src="/bitrix/templates/arcada_artem/images/cursor_act.png" alt="cursor_act"><span>Игры</span><span class="lfmenu_r">50</span></a>
                    <a href="/catalog/22/"><img src="/bitrix/templates/arcada_artem/images/cursor_act.png" alt="cursor_act"><span>Приставки</span><span class="lfmenu_r">50</span></a>
                </div>	
            </div>	
        </div>
        <h3 id="left-menu-item-09" class=""><table class=lm_img_wrap><tr><td><img src="/bitrix/templates/arcada_artem/images/nintendo.png" alt="nintendo"></td></tr></table><span><b>Nintendo</b></span></h3>
        <div class="lfmenu">
            <div class="left-panel-bottom accordion">
                <h3 id="left-menu-item-09" class=""><table class=lm_img_wrap><tr><td><img src="/bitrix/templates/arcada_artem/images/nintendo_wii.png" alt="nintendo_wii"></td></tr></table><span><b>Nintendo Wii</b></span></h3>
                <div class="lfmenu">
                    <a href="/catalog/21/"><img src="/bitrix/templates/arcada_artem/images/cursor_act.png" alt="cursor_act"><span>Игры</span><span class="lfmenu_r">50</span></a>
                    <a href="/catalog/22/"><img src="/bitrix/templates/arcada_artem/images/cursor_act.png" alt="cursor_act"><span>Приставки</span><span class="lfmenu_r">50</span></a>
                </div>										
                <h3 id="left-menu-item-09" class=""><table class=lm_img_wrap><tr><td><img src="/bitrix/templates/arcada_artem/images/nintendo_wii.png" alt="nintendo_wii_u"></td></tr></table><span><b>Nintendo Wii U</b></span></h3>
                <div class="lfmenu">
                    <a href="/catalog/34/"><img src="/bitrix/templates/arcada_artem/images/cursor_act.png" alt="cursor_act"><span>Игры</span><span class="lfmenu_r">50</span></a>
                    <a href="/catalog/29/"><img src="/bitrix/templates/arcada_artem/images/cursor_act.png" alt="cursor_act"><span>Приставки</span><span class="lfmenu_r">50</span></a>
                </div>										
                <h3 id="left-menu-item-09" class=""><table class=lm_img_wrap><tr><td><img src="/bitrix/templates/arcada_artem/images/nintendo_3ds.png" alt="nintendo_3ds"></td></tr></table><span><b>Nintendo 3DS</b></span></h3>
                <div class="lfmenu">
                    <a href="/catalog/21/"><img src="/bitrix/templates/arcada_artem/images/cursor_act.png" alt="cursor_act"><span>Игры</span><span class="lfmenu_r">50</span></a>
                    <a href="/catalog/22/"><img src="/bitrix/templates/arcada_artem/images/cursor_act.png" alt="cursor_act"><span>Приставки</span><span class="lfmenu_r">50</span></a>
                </div>	
            </div>	
        </div>	
        <h3 id="left-menu-item-013" class=""><span><b>Другие</b></span></h3>
        <div class="lfmenu">
            <a href="/catalog/58/"><img src="/bitrix/templates/arcada_artem/images/cursor_act.png" alt="cursor_act"><span>Другие приставки</span></a>
            <a href="/catalog/59/"><img src="/bitrix/templates/arcada_artem/images/cursor_act.png" alt="cursor_act"><span>Игры</span><span class="lfmenu_r">50</span></a>
            <a href="/catalog/62/"><img src="/bitrix/templates/arcada_artem/images/cursor_act.png" alt="cursor_act"><span>Аксессуары</span><span class="lfmenu_r">50</span></a>
        </div>
    </div>					
</div>







