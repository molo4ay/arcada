<div id="left-menu">
					<!-- 1-ый блок -->
					<div class="left-panel">
						<div class="left-panel-top">
							<span>XBOX</span>
						</div>
						<div class="left-panel-bottom accordion">
							<h3>Игровые приставки XBOX</h3>
							<div class="lfmenu">
								<a href="#"><span>Игровые приставки XBOX</span></a><br>
								<a href="#"><span>Игровые приставки XBOX</span></a>
							</div>
							<h3>Аксессуары</h3>
							<div  class="lfmenu">
								<a href="#"><span>Аксессуары</span></a><br>
								<a href="#"><span>Аксессуары</span></a>
							</div>
							<h3>Видео игры для XBOX</h3>
							<div class="lfmenu">
								<a href="#"><span>Видео игры для XBOX</span></a><br>
								<a href="#"><span>Видео игры для XBOX</span></a>
							</div>
							<h3>Распродажа</h3>
							<div class="lfmenu">
								<a href="#"><span>Распродажа</span></a><br>
								<a href="#"><span>Распродажа</span></a>
							</div>
						</div>
					</div>
					<!-- ///1-ый блок -->
					<!-- 2-ый блок -->
					<div class="left-panel">
						<div class="left-panel-top">
							<span>Play Station</span>
						</div>
						<div class="left-panel-bottom accordion">
							<h3>Игровые приставки SP</h3>
							<div class="lfmenu">
								<a href="#"><span>Игровые приставки SP</span></a><br>
								<a href="#"><span>Игровые приставки SP</span></a>
							</div>
							<h3>Аксессуары</h3>
							<div class="lfmenu">
								<a href="#"><span>Аксессуары</span></a><br>
								<a href="#"><span>Аксессуары</span></a>
							</div>
							<h3>Видео игры для SP</h3>
							<div class="lfmenu">
								<a href="#"><span>Видео игры для SP</span></a><br>
								<a href="#"><span>Видео игры для SP</span></a>
							</div>
							<h3>Распродажа</h3>
							<div class="lfmenu">
								<a href="#"><span>Распродажа</span></a><br>
								<a href="#"><span>Распродажа</span></a>
							</div>
						</div>
					</div>
					<!-- ///2-ый блок -->
					<!-- 3-ый блок -->
					<div class="left-panel">
						<div class="left-panel-top">
							<span>Nintendo</span>
						</div>
						<div class="left-panel-bottom accordion">
							<h3>Игровые приставки Nintendo</h3>
							<div class="lfmenu">
								<a href="#"><span>Игровые приставки Nintendo</span></a><br>
								<a href="#"><span>Игровые приставки Nintendo</span></a>
							</div>
							<h3>Аксессуары</h3>
							<div class="lfmenu">
								<a href="#"><span>Аксессуары</span></a><br>
								<a href="#"><span>Аксессуары</span></a>
							</div>
							<h3>Видео игры для Nintendo</h3>
							<div class="lfmenu">
								<a href="#"><span>Видео игры для Nintendo</span></a><br>
								<a href="#"><span>Видео игры для Nintendo</span></a>
							</div>
							<h3>Распродажа</h3>
							<div class="lfmenu">
								<a href="#"><span>Распродажа</span></a><br>
								<a href="#"><span>Распродажа</span></a>
							</div>
						</div>
					</div>
					<!-- ///3-ый блок -->
					<!-- 4-ый блок -->
					<div class="left-panel">
						<div class="left-panel-top">
							<span>Другие приставки</span>
						</div>
						<div class="left-panel-bottom accordion">
							<h3>Игровые приставки PSP</h3>
							<div class="lfmenu">
								<a href="#"><span>Игровые приставки PSP</span></a><br>
								<a href="#"><span>Игровые приставки PSP</span></a>
							</div>
							<h3>Аксессуары</h3>
							<div class="lfmenu">
								<a href="#"><span>Аксессуары</span></a><br>
								<a href="#"><span>Аксессуары</span></a>
							</div>
							<h3>Видео игры для PSPX</h3>
							<div class="lfmenu">
								<a href="#"><span>Видео игры для PSP</span></a><br>
								<a href="#"><span>Видео игры для PSP</span></a>
							</div>
							<h3>Распродажа</h3>
							<div class="lfmenu">
								<a href="#"><span>Распродажа</span></a><br>
								<a href="#"><span>Распродажа</span></a>
							</div>
						</div>
					</div>
					<!-- ///4-ый блок -->
					<!-- 5-ый блок -->
					<div class="left-panel">
						<div class="left-panel-top">
							<span>Настольные игры</span>
						</div>
						<div class="left-panel-bottom accordion">
							<h3>Для детей</h3>
							<div class="lfmenu">
								<a href="#"><span>Для детей</span></a><br>
								<a href="#"><span>Для детей</span></a>
							</div>
							<h3>Для подростков</h3>
							<div class="lfmenu">
								<a href="#"><span>Для подростков</span></a><br>
								<a href="#"><span>Для подростков</span></a>
							</div>
							<h3>Для взрослых</h3>
							<div class="lfmenu">
								<a href="#"><span>Для взрослых</span></a><br>
								<a href="#"><span>Для взрослых</span></a>
							</div>
							<h3>Распродажа</h3>
							<div class="lfmenu">
								<a href="#"><span>Распродажа</span></a><br>
								<a href="#"><span>Распродажа</span></a>
							</div>
						</div>
					</div>
					<!-- ///5-ый блок -->
					<!-- 6-ый блок -->
					<div class="left-panel">
						<div class="left-panel-top">
							<span>Программы</span>
						</div>
						<div class="left-panel-bottom accordion">
							<h3>Для детей</h3>
							<div class="lfmenu">
								<a href="#"><span>Для детей</span></a><br>
								<a href="#"><span>Для детей</span></a>
							</div>
							<h3>Для подростков</h3>
							<div class="lfmenu">
								<a href="#"><span>Для подростков</span></a><br>
								<a href="#"><span>Для подростков</span></a>
							</div>
							<h3>Для взрослых</h3>
							<div class="lfmenu">
								<a href="#"><span>Для взрослых</span></a><br>
								<a href="#"><span>Для взрослых</span></a>
							</div>
							<h3>Распродажа</h3>
							<div class="lfmenu">
								<a href="#"><span>Распродажа</span></a><br>
								<a href="#"><span>Распродажа</span></a>
							</div>
						</div>
					</div>
					<!-- ///6-ый блок -->
					<!-- В контакте -->
					<div class="vk">
						<p>Мы вконтакте</p>
						<iframe name="fXD9e4d9" frameborder="0" src="http://vk.com/widget_community.php?app=0&amp;width=200px&amp;_ver=1&amp;gid=37701246&amp;mode=0&amp;color1=&amp;color2=&amp;color3=&amp;height=290&amp;url=http%3A%2F%2Farcade.ya1.ru%2Fold%2F&amp;14082fc56dd" width="200" height="200" scrolling="no" id="vkwidget1" style="overflow: hidden; width:213px; height: 290px;"></iframe>
					</div>
					<!-- ////В контакте -->
				</div>





