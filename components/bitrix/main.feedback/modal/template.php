<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<script type="text/javascript">
	$(function() {
		var $dialog = $("#buzzer_form_div");
		
		$("#bz").click(function() {
			$dialog.dialog({
				width: 350,
				height: 450,
				modal: true,
				dialogClass: "buzzer",
				resizable: false,
				show: {
					effect: "blind",
					duration: 1000
				},
				hide: {
					effect: "explode",
					duration: 1000
				}				
			});
		});
		
		$dialog.on('click', '.closer', function() {
			$dialog.dialog('close');
		});
		
		<?if (isset($_REQUEST['feedback']) || isset($_REQUEST['success'])): ?>
			$("#bz").click();
		<? endif; ?>
	});
</script>

<div id="buzzer_form_div" style="display: none;">
	<form id="buzzer_form" action="/?feedback=ok" method="POST">
		<a href="javascript:void(0);" class="closer"></a>
		<input type="hidden" name="submit" value="feedback" />
		<?= bitrix_sessid_post() ?>
		<h2>МЫ ВАМ ПЕРЕЗВОНИМ</h2>
		<? if (!empty($arResult["ERROR_MESSAGE"])): ?>
			<? foreach ($arResult["ERROR_MESSAGE"] as $v): ?>
				<? ShowError($v); ?>
			<? endforeach; ?>
		<? endif; ?>		
		<? if (strlen($arResult["OK_MESSAGE"]) > 0): ?>
			<? ShowNote($arResult["OK_MESSAGE"]); ?>
		<? else: ?>
			<input class="zv-text zvon-inp" placeholder="Ваше Имя *" required="required" type="text" name="user_name" value="<?=$arResult["AUTHOR_NAME"]?>" />
			<input class="zv-text zvon-inp" placeholder="Ваш номер телефона *" required="required" type="text" name="MESSAGE" value="<?=$arResult["MESSAGE"]?>" />
			<input class="zv-text zvon-inp" placeholder="Ваш email *" required="required" type="email" name="user_email" value="<?=$arResult["AUTHOR_EMAIL"]?>" />

			<?if($arParams["USE_CAPTCHA"] == "Y"):?>
				<div id="buzzer_captcha" >
					<input type="hidden" name="captcha_sid" value="<?= $arResult["capCode"] ?>" />
					<input class="zv-text zvon-test" required="required" type="text" name="captcha_word" size="30" maxlength="50" value="" />
					<img class="captcha" src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["capCode"] ?>" width="180" height="36" alt="ban2-activ" />
				</div>
			<?endif;?>

			<input class="zv-send" src="<?= SITE_TEMPLATE_PATH ?>/images/zvonok-but-pas.png" type="image" alt="OK" />
		<? endif; ?>
	</form>
</div>