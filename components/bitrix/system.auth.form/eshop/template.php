<? if ($arResult["FORM_TYPE"] == "login"): ?>
	<div class="login-on">
		<p>авторизация:</p>
		<form id="authorisation" name="form_auth" method="post" target="_top" action="<?= $arResult["AUTH_URL"] ?>">
			<input type="hidden" name="AUTH_FORM" value="Y" />
			<input type="hidden" name="TYPE" value="AUTH" />
			<? if (strlen($arResult["BACKURL"]) > 0): ?>
				<input type="hidden" name="backurl" value="/" />
			<? endif ?>
			<? foreach ($arResult["POST"] as $key => $value): ?>
				<input type="hidden" name="<?= $key ?>" value="<?= $value ?>" />
			<? endforeach ?>

			<div>
				<input id="autoris-button" src="<?= SITE_TEMPLATE_PATH ?>/images/ok.png" type="image" alt="OK" onclick="$(this).parents('form:first').submit();">
			</div>

			<input id="autoris-name" type="text" name="USER_LOGIN" size="15" maxlength="30">
			<input id="autoris-password" type="password" name="USER_PASSWORD" size="15" maxlength="30">

			<? if ($arResult["CAPTCHA_CODE"]): ?>
				<input type="hidden" name="captcha_sid" value="<? echo $arResult["CAPTCHA_CODE"] ?>" />
				<img src="/bitrix/tools/captcha.php?captcha_sid=<? echo $arResult["CAPTCHA_CODE"] ?>" width="180" height="40" alt="CAPTCHA" /></td>
				<? echo GetMessage("AUTH_CAPTCHA_PROMT") ?>:
				<input class="bx-auth-input" type="text" name="captcha_word" maxlength="50" value="" size="15" />
			<? endif; ?>

			<? if ($arResult["STORE_PASSWORD"] == "Y"): ?>
					<!-- input type="checkbox" id="USER_REMEMBER" name="USER_REMEMBER" value="Y" /><label for="USER_REMEMBER">&nbsp;<?= GetMessage("AUTH_REMEMBER_ME") ?></label -->
			<? endif ?>

			<div class="author-rp">
				<? if ($arParams["NOT_SHOW_LINKS"] != "Y"): ?>
					<noindex>
						<a href="<?= $arResult["AUTH_FORGOT_PASSWORD_URL"] ?>" rel="nofollow"><?= GetMessage("AUTH_FORGOT_PASSWORD_2") ?></a>
					</noindex>
				<? endif ?>
				<? if ($arParams["NOT_SHOW_LINKS"] != "Y" && $arResult["NEW_USER_REGISTRATION"] == "Y" && $arParams["AUTHORIZE_REGISTRATION"] != "Y"): ?>
					<noindex>
						<a href="<?= $arResult["AUTH_REGISTER_URL"] ?>" rel="nofollow"><?= GetMessage("AUTH_REGISTER") ?></a>
					</noindex>
				<? endif ?>
			</div>
		</form>
	</div>	
<? else: ?>
	<div class="login-off">
		<p>вы можете посмотреть:</p>
		<div class="author-rp">
			<a href="/personal/order/">Мои заказы</a>
			<a href="/personal/">Личный кабинет</a>
			<br />
			<a href="/?logout=yes">Выход</a>
		</div>
	</div>
<? endif; ?>