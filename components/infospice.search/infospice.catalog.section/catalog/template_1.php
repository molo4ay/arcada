<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<script type="text/javascript">
$(function(){ 
	$items = $(".result-items-boxes2 .item");
	countItems = $items.size();
	var i=0;
	while(i < countItems){
		min = i; max = i + 4;
	    if(max > countItems) max = countItems; 
	    $resizeItems = $(".result-items-boxes2 .item").slice(min, max);
	    var maxHeightImage = 0;
	    var maxHeightName = 0; 
	    var maxHeightPrice = 0;
	    $resizeItems.each(function(){
		     var heightImage, heightName, heightPrice; heightImage = $("[align=image]", this).height();
		     heightName = $(".description", this).height();
		     heightPrice = $(".price-label", this).height();
		     maxHeightImage = Math.max(maxHeightImage, heightImage);
		     maxHeightName = Math.max(maxHeightName, (heightName)); 
		     maxHeightPrice = Math.max(maxHeightPrice, heightPrice); }) 
		     $("[align=image]", $resizeItems).height(maxHeightImage); 
             $(".description", $resizeItems).height(maxHeightName+5);
             $(".price-label", $resizeItems).height(maxHeightPrice); i += 4; 
    	}
	})
</script>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>
<div class="result-items result-items-boxes2">
	<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
		<div class="item">
			<div align="image">
				<?if(!empty($arElement["PREVIEW_PICTURE"]["SRC"])):?>
					<a href="<?=$arElement["~DETAIL_PAGE_URL"]?>"><img border="0" src="<?=$arElement["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arElement["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arElement["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" /></a>
				<?elseif(is_array($arElement["DETAIL_PICTURE"])):?>
					<a href="<?=$arElement["~DETAIL_PAGE_URL"]?>"><img border="0" src="<?=$arElement["DETAIL_PICTURE"]["SRC"]?>" width="<?=$arElement["DETAIL_PICTURE"]["WIDTH"]?>" height="<?=$arElement["DETAIL_PICTURE"]["HEIGHT"]?>" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" /></a>
				<?else:?>
					<a href="<?=$arElement["~DETAIL_PAGE_URL"]?>"><img src="<?=$templateFolder?>/images/none.png" alt="none"></a>
				<?endif?>
			</div>
			<div class="description">
				<p><strong><a href="<?=$arElement["~DETAIL_PAGE_URL"]?>"><?echo $arElement["NAME"]?></a></strong></p>
			</div>
			<?foreach($arElement["PRICES"] as $code=>$arPrice):?>
				<?if($arPrice["CAN_ACCESS"]):?>
					<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
						<strong class="price-label"><?=$arPrice["VALUE_NOVAT"]?></strong>
						<strong class="price-label"><?=$arPrice["DISCOUNT_VALUE"]?></strong>
					<?else:?>
						<strong class="price-label"><?=$arPrice["PRINT_VALUE"]?></strong>
					<?endif;?>
				<?endif;?>
			<?endforeach;?>
		</div>
	<?endforeach;?>
</div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>
