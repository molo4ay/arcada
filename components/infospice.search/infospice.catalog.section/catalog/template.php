<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>

<div class="suggest">
	<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
		<? if ($cell && $cell % 3 === 0): ?>
			</div><div class="suggest">
		<? endif; ?>
		<? $PRICE = $arElement["PRICES"][$arParams["PRICE_CODE"][0]]; ?>
		<!-- <?= $cell; ?>-ый рекомендуемый -->
		<div class="suggest-block" itemscope="" itemtype="http://schema.org/Product">
			<a href="<?=$arElement["~DETAIL_PAGE_URL"]?>" title="<?echo $arElement["NAME"]?>"><?echo $arElement["NAME"]?></a> 							 
			<div class="product">
				<a href="<?=$arElement["~DETAIL_PAGE_URL"]?>" title="<?echo $arElement["NAME"]?>">
					<img class="bm-act" src="<?=$arElement["DETAIL_PICTURE"]["SRC"]?>" alt="<?echo $arElement["NAME"]?>">
				</a>
			</div>
			<div class="feature">
				<div class="price-s">
					<? if ($PRICE["DISCOUNT_VALUE"] != $PRICE["VALUE"]): ?>
						<div class="price-old">
							<p><span><?=$PRICE["VALUE"]?></span> руб.</p>
						</div>
					<? endif; ?>
					<div class="price">
						<p><span><?=$PRICE["DISCOUNT_VALUE"]?></span> руб.</p>
					</div>
				</div>
				<div class="buy">
					<a href="/catalog/<?=$arElement["IBLOCK_SECTION_ID"]?>/<?=$arElement["ID"]?>/?action=ADD2BASKET&id=<?=$arElement["ID"]?>" rel="nofollow">&nbsp;</a>
				</div>
			</div>
		</div>
		<!-- ///<?= $cell; ?>-ый рекомендуемый -->
	<?endforeach;?>
</div>

<br />

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>
