<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach ($arResult["ITEMS"] as $key => $arElement) 
{

    //echo '<pre>'; print_r( $arResult ); echo '</pre>';

    if ( !isset($arResult["ITEMS"][$key]["PREVIEW_PICTURES"])) 
    {
            $arFilter = array(array("name" => "sharpen", "precision" => 15));
            $arFileTmp = CFile::ResizeImageGet(
                $arElement["DETAIL_PICTURE"],
                array("width" => 140, "height" => 140),
                BX_RESIZE_IMAGE_PROPORTIONAL,
                true, $arFilter
            );

            $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = array(
                "SRC" => $arFileTmp["src"],
                'WIDTH' => $arFileTmp["width"],
                'HEIGHT' => $arFileTmp["height"],
            );
            $arResult["ITEMS"][$key]["DETAIL_PAGE_URL"] = $url;
    }

}

?>
