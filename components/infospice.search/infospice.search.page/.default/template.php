<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div id="search-module">
<form action="" method="get" class="search-form">
	<fieldset>
		<input type="text" class="text" name="q" value="<?=$arResult["REQUEST"]["QUERY"]?>" />
		<input type="submit" class="submit" value="<?=GetMessage("SEARCH_GO")?>" /> <input
		type="hidden" name="how"
		value="<?echo $arResult["REQUEST"]["HOW"]=="d"? "d": "r"?>" />
	</fieldset>
</form>

<?if($arResult["REQUEST"]["QUERY"] === false && $arResult["REQUEST"]["TAGS"] === false):?>
	<? // do something ... ?>
	
<?elseif(count($arResult["SEARCH"]) < 1):?>
	<?ShowNote(GetMessage("SEARCH_NOTHING_TO_FOUND"));?>
	
<?else:?>

	<?
	foreach($arResult["SEARCH"] as $arr){
		if(is_numeric($arr["ITEM_ID"])){
			$arr_sort[$arr["PARAM2"]][] = $arr["ITEM_ID"];
		}
	}
	/*echo "<pre>";
	print_r($staticPage);
	echo "</pre>";*/

	foreach($arr_sort as $k=>$v){
		$iblockArray[] = $k;
		$countElement[$k] = count($v);
	}
	$static = "";
	if(count($arResult["SEARCH_STATIC"]) > 0){
		$static = "Y";
		$static_count = count($arResult["SEARCH_STATIC"]);
	}

	# ������ ����������
	$APPLICATION->IncludeComponent("infospice.search:infospice.iblock.list", $arParams["TEMPLATE_SECTION"], array(
		"FILTER_NAME" => $iblockArray,
		"COUNT_ELEMENT" => $countElement,
		"ARRAY_ELEMENT" => $arr_sort,
		"STATIC" => $static,
		"STATIC_COUNT" => $static_count
		),
		$component,array("HIDE_ICONS" => "Y")
	);

?>
<?endif;?>



<?if($_GET["page"] == "static"):?>

	<?if(count($arResult["SEARCH_STATIC"]) > 0):?>
		<?foreach($arResult["SEARCH_STATIC"] as $item):?>
			<p><a href="<?echo $item["URL"];?>"><?echo $item["TITLE"];?></a></p>
			<p><?echo $item["BODY_FORMATED"];?></p>
		<?endforeach;?>
		<?echo $arResult["NAV_STRING_STATIC"]?>
	<?endif;?>
	
<?else:?>
	<?		

	# ������ ���������
	if($_GET["iblock"]){
		CModule::IncludeModule("catalog");
		# �������� ������
		foreach($arr_sort as $key => $value){
			# ���� ���� �������� � ������� section
			# �� ������� �������� ������ ��� ����� �������
			if($_GET["section"])
			{
				# �� ��������� ��������, ������ ������ �������
				
				/*$sectionSelect = arraySort($value);
				$filter = $sectionSelect[$_GET["section"]];*/
				
				# �������� ������, � ������� ���� ��������� ��������
				foreach($value as $id){
					$arSelect = Array("ID","IBLOCK_SECTION_ID");
					$arFilter = Array("ID"=>$id);
					$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
					if($arrRes = $res->Fetch())
					{
						$sectionSelect[$arrRes["IBLOCK_SECTION_ID"]][] = $arrRes["ID"];	
					}
				}

				$filter = $sectionSelect[$_GET["section"]];
			}
			else{
				# �������� ������ � ���������� ���������
				if($key  == $_GET["iblock"])
				{
					$filter = $value;
				}
			}
		}
		$get_iblock = $_GET["iblock"];
		$GLOBALS['arrFilter']  = array(
			"ID" => $filter
		);
	}
	else{
		# ���� � ��������� �������� �������
		if(CModule::IncludeModule("catalog")){
			foreach($arr_sort as $iblock_id => $value){
				$checkCatalog = CCatalog::GetByID($iblock_id);
				if(is_array($checkCatalog)){
					# �� �������� �������� ���������
					$catalog_id = $iblock_id;
					break;
				}
				else {
					# ���� �� �� �������� ���������, �������� 
					# ��� �� ������������ � ���� �������� �����������
					$res = CCatalog::GetList(
						array(),
						array( "PRODUCT_IBLOCK_ID" => $iblock_id )
					);
					if( $arIblock = $res->Fetch()){
						$catalog_id = $iblock_id;
						break;
					}
				}
			}
		}

		# ������ ������� �� ������� $arr_sort ������ ����
		if(!empty($catalog_id)){
			$GLOBALS['arrFilter']  = array(
				"ID" => $arr_sort[$catalog_id]
			);
			$get_iblock = $catalog_id;
		}
	}
	# ��������� ��������
	if(!empty($get_iblock)){
		if (CModule::IncludeModule("catalog")){
			# ���������� - ������� ��� ������� ��������
			$checkCatalog = CCatalog::GetByID($get_iblock);
			if(is_array($checkCatalog)){
				$template = $arParams["TEMPLATE_CATALOG"];
			}
			else{
                # ����� �� - ��������� �������� �� �� �������� ���������
                $res = CCatalog::GetList( 
                        array(),
                        array( "PRODUCT_IBLOCK_ID" => $get_iblock )
                );
                if ( $arIBlock = $res->Fetch()) {
				    $template = $arParams["TEMPLATE_CATALOG"];
                }
                else {
                    $template = $arParams["TEMPLATE_IBLOCK"];
                }
			}
        }
        else {
			$template = $arParams["TEMPLATE_IBLOCK"];
		}
	}
	
	$APPLICATION->IncludeComponent(
	"infospice.search:infospice.catalog.section",
	$template,
	Array(
		"AJAX_MODE" => "N",
		"IBLOCK_ID" => $get_iblock,
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_CODE" => "",
		"SECTION_USER_FIELDS" => "",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "asc",
		"FILTER_NAME" => "arrFilter",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "Y",
		"SECTION_URL" => "",
		"DETAIL_URL" => "",
		"BASKET_URL" => "/personal/basket.php",
		"ACTION_VARIABLE" => "action",
		"PORDUCT_ID_VARIABLE" => "id",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"META_KEYWORDS" => "-",
		"META_DESCRIPTION" => "-",
		"BROWSER_TITLE" => "-",
		"ADD_SECTIONS_CHAIN" => "N",
		"DISPLAY_COMPARE" => "N",
		"SET_TITLE" => "Y",
		"SET_STATUS_404" => "N",
		"PAGE_ELEMENT_COUNT" => $arParams["PAGE_IBLOCK_COUNT"],
		"LINE_ELEMENT_COUNT" => "1",
		"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
		"PRICE_CODE" => $arParams["PRICE_CODE"],
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_PROPERTIES" => "",
		"USE_PRODUCT_QUANTITY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_NOTES" => "",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "Y",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGE_TITLE" => $arParams[ "PAGE_TITLE" ],
		"PAGER_TITLE" => $arParams[ "PAGER_TITLE" ],
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "infospice.search.pagenav",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	),
	$component,array("HIDE_ICONS" => "Y")
	);
	?>

<?endif;?>

</div>
