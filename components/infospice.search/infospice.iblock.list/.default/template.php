<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
	$iblockId    = ( !empty( $_GET[ "iblock" ]  )) ? ( int )$_GET[ "iblock" ]  : 0;
	$sectionId   = ( !empty( $_GET[ "section" ] )) ? ( int )$_GET[ "section" ] : 0;
	$userQuery   = ( !empty( $_GET[ "q" ] ))       ? trim( $_GET[ "q" ])       : "";
	$pageType    = ( !empty( $_GET[ "page" ] ))    ? trim( $_GET[ "page" ])    : "";
	$isStatic    = ( boolean )( $pageType == "static" );
	
	


	$url  = $APPLICATION->GetCurPage();
	$url .= "?q=".$userQuery; 
		
	if( $iblockId > 0 or $sectionId > 0 ) {
		$url .= "&iblock=".$iblockId;
	}
	

?>
	<a class="title" href="<?echo $url?>">&larr;</a>
	<br /><br />

<?
		$sectlist = array();

		foreach ($arResult["SECTION"] as $k=>$section) {
			$res = CIBlockSection::GetByID($section["ID"]);
			$arSection = $res->GetNext();
			
			$par = $arSection["IBLOCK_SECTION_ID"];
			$depth = $arSection["DEPTH_LEVEL"];
			//echo $section['NAME']."<br />";
			$sectlist[$depth][] = array(
				"ID" => $arSection["ID"],
				"NAME" => $arSection["NAME"],
				"PARENT" => $arSection["IBLOCK_SECTION_ID"],
				"COUNT" => $section["COUNT"],
			);
			while ($depth > 2) {
				$arFilter = Array("ID" => $par);
				$db_list = CIBlockSection::GetList(Array(), $arFilter, false);
				$parent = $db_list->Fetch();
				$par = $parent["IBLOCK_SECTION_ID"];
				$depth = $parent["DEPTH_LEVEL"];
				$data = array(
					"ID" => $parent["ID"],
					"NAME" => $parent["NAME"],
					"PARENT" => $parent["IBLOCK_SECTION_ID"],
				);
				if (!in_array($data, $sectlist[$depth])) $sectlist[$depth][] = $data;
			}
		}

	 	$query = $_SERVER["QUERY_STRING"];
	 	$query = preg_replace("/&section=\d*/", "", $query);

		echo "<div id='search-category-list'>";
	 	echo "<ul class='depth2'>";
	 	if ($sectlist[2]) {
	 		foreach ($sectlist[2] as $k2=>$v2) {
	 			echo "<li>";
	 			echo ($v2["COUNT"] ? "<a".($v2["ID"] == $sectionId ? " class='active'" : "")." href='?{$query}&section=".$v2["ID"]."'>".$v2["NAME"]."</a> (".$v2["COUNT"].")" : "<span>".$v2["NAME"]."</span>");
	 			$depth3 = "";
	 			$parent2 = $v2["ID"];
		 		foreach ($sectlist[3] as $k3=>$v3) {
		 			if ($v3["PARENT"] == $parent2) {
			 			$depth3 .= "<li>";
			 			$depth3 .= ($v3["COUNT"] ? "<a".($v3["ID"] == $sectionId ? " class='active'" : "")." href='?{$query}&section=".$v3["ID"]."'>".$v3["NAME"]."</a> (".$v3["COUNT"].")" : "<span>".$v3["NAME"]."</span>");
			 			$depth4 = "";
	 					$parent3 = $v3["ID"];
				 		foreach ($sectlist[4] as $k4=>$v4) {
				 			if ($v4["PARENT"] == $parent3) {
					 			$depth4 .= "<li>";
					 			$depth4 .= ($v4["COUNT"] ? "<a".($v4["ID"] == $sectionId ? " class='active'" : "")." href='?{$query}&section=".$v4["ID"]."'>".$v4["NAME"]."</a> (".$v4["COUNT"].")" : "<span>".$v4["NAME"]."</span>");
					 			$depth5 = "";
	 							$parent4 = $v4["ID"];
						 		foreach ($sectlist[5] as $k5=>$v5) {
						 			if ($v5["PARENT"] == $parent4) {
							 			//$depth5 .= "<li>";
							 			$depth5 .= ($v5["COUNT"] ? "<a".($v5["ID"] == $sectionId ? " class='active'" : "")." href='?{$query}&section=".$v5["ID"]."'>".$v5["NAME"]."</a> (".$v5["COUNT"].")" : "<span>".$v5["NAME"]."</span>");
							 			//$depth5 .= "</li>";
							 		}
						 		}
						 		//if ($depth5) $depth4 .= "<ul class='depth5'>".$depth5."</ul>";
						 		if ($depth5) $depth4 .= "<span class='depth5'>".$depth5."</span>";
					 			$depth4 .= "</li>";
					 		}
				 		}
				 		if ($depth4) $depth3 .= "<ul class='depth4'>".$depth4."</ul>";
			 			$depth3 .= "</li>";
			 		}
		 		}
		 		if ($depth3) echo "<ul class='depth3'>".$depth3."</ul>";
	 			echo "</li>";
	 		}
	 	}
	 	echo "</ul>";
	 	echo "</div>";
?>	

<div class="category-links category-links2" style="display: none;">
	
	<div class="list-holder">
		<ul>
			<?
			foreach($arResult["SECTION"] as $items) 
			{
			
				$active = "";
				if($arResult["SECTION_URL"] == "Y"){
					if($items['ID'] == $sectionId){
						$active = "active";
					}
				} else {
					if($items['ID'] == $iblockId){
						$active = "active";
					}
				}
				
				
				$url  = $APPLICATION->GetCurPage();
				$url .= "?q=".$userQuery;  
				$name = $items['NAME'];
				
				
				if ( $arResult["SECTION_URL"] == "Y" ) {
					if ( $iblockId > 0 )       $url   .= "&iblock=".$iblockId;
					if ( $items['ID'] > 0 )    $url   .= "&section=".$items['ID'];
				}
				else {
					if ( $items['ID'] > 0 )    $url   .= "&iblock=".$items['ID'];	
				}
				
				if ( $items['COUNT'] > 0 )     $count  = " (".$items['COUNT'].")";
				
				?>
				<li class="<?echo $active?>"><span class="co-r"><span class="co-l">
					<a href="<?=$url?>"><?=$name?></a><?=$count?>
					</span></span>
				</li>
				<?
			}
			
			
			
			if ( $arParams["STATIC"] == "Y" ) 
			{ 
				$active = ( $isStatic ) ? "active" : "";
				
				$url  = $APPLICATION->GetCurPage();
				$url .= "?q=".$userQuery; 
				$url .= "&page=static";
				$name = GetMessage('STATIC_PAGE');
				
				if ( $arParams["STATIC_COUNT"] > 0 )     $count  = " (".$arParams["STATIC_COUNT"].")";
				
				?>
				<li class="<?echo $active?>"><span class="co-r"><span class="co-l">
					<a href="<?=$url?>"><?=$name?></a><?=$count?>
					</span></span>
				</li>
				<?
			}
			?>
		</ul>
	</div>
</div>

