<!DOCTYPE html>
<html>
  <head>
    <title><? $APPLICATION->ShowTitle() ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="shortcut icon" type="image/x-icon" href="<?= SITE_TEMPLATE_PATH ?>/favicon.ico">
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
		<?
		//$APPLICATION->ShowHead();
		echo '<meta http-equiv="Content-Type" content="text/html; charset=' . LANG_CHARSET . '"' . (true ? ' /' : '') . '>' . "\n";
		$APPLICATION->ShowMeta("robots", false, true);
		$APPLICATION->ShowMeta("keywords", false, true);
		$APPLICATION->ShowMeta("description", false, true);
		$APPLICATION->ShowCSS(true, true);
		?>
		<link rel="stylesheet" type="text/css" href="<?= CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH . "/colors.css") ?>" />
		<? if (strpos($_SERVER['HTTP_USER_AGENT'], "MSIE") && !strpos($_SERVER['HTTP_USER_AGENT'], "MSIE 10.0")): ?>
			<link rel="stylesheet" type="text/css" href="<?= CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH . "/ie.css") ?>"/>
		<? endif ?>
		<?
		$APPLICATION->ShowHeadStrings();
		$APPLICATION->ShowHeadScripts();
		?>
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">	
		
		<? CJSCore::Init(array("jquery")); ?>
		<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
		
		<?
		//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/slides.min.jquery.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/script.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.carouFredSel-5.6.4-packed.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.cookie.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.slideViewerPro.1.5.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.timers.js");
		if ($wizTemplateId == "eshop_horizontal"):
			$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/bx.topnav.js");
			$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/hnav.js");
		endif;
		?>
		<script type="text/javascript">
			if (document.documentElement) {
				document.documentElement.id = "js"
			}
		</script>
		<script src="<?= SITE_TEMPLATE_PATH ?>/js/grayscale.js"></script>
		
		<!--[if IE]>
		<style type="text/css">
			#fancybox-loading.fancybox-ie div	{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/js/fancybox/fancy_loading.png', sizingMethod='scale'); }
			.fancybox-ie #fancybox-close		{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/js/fancybox/fancy_close.png', sizingMethod='scale'); }
			.fancybox-ie #fancybox-title-over	{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/js/fancybox/fancy_title_over.png', sizingMethod='scale'); zoom: 1; }
			.fancybox-ie #fancybox-title-left	{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/js/fancybox/fancy_title_left.png', sizingMethod='scale'); }
			.fancybox-ie #fancybox-title-main	{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/js/fancybox/fancy_title_main.png', sizingMethod='scale'); }
			.fancybox-ie #fancybox-title-right	{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/js/fancybox/fancy_title_right.png', sizingMethod='scale'); }
			.fancybox-ie #fancybox-left-ico		{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/js/fancybox/fancy_nav_left.png', sizingMethod='scale'); }
			.fancybox-ie #fancybox-right-ico	{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/js/fancybox/fancy_nav_right.png', sizingMethod='scale'); }
			.fancybox-ie .fancy-bg { background: transparent !important; }
			.fancybox-ie #fancy-bg-n	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/js/fancybox/fancy_shadow_n.png', sizingMethod='scale'); }
			.fancybox-ie #fancy-bg-ne	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/js/fancybox/fancy_shadow_ne.png', sizingMethod='scale'); }
			.fancybox-ie #fancy-bg-e	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/js/fancybox/fancy_shadow_e.png', sizingMethod='scale'); }
			.fancybox-ie #fancy-bg-se	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/js/fancybox/fancy_shadow_se.png', sizingMethod='scale'); }
			.fancybox-ie #fancy-bg-s	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/js/fancybox/fancy_shadow_s.png', sizingMethod='scale'); }
			.fancybox-ie #fancy-bg-sw	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/js/fancybox/fancy_shadow_sw.png', sizingMethod='scale'); }
			.fancybox-ie #fancy-bg-w	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/js/fancybox/fancy_shadow_w.png', sizingMethod='scale'); }
			.fancybox-ie #fancy-bg-nw	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/js/fancybox/fancy_shadow_nw.png', sizingMethod='scale'); }
		</style>
		<![endif]-->
	</head>
	<div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
	<body id="mybody">
		<!-- div id="casper"></div -->
		<!-- Задий фон -->
		<div id="backgrnd">
			<div>
				<a href="#" class="fbutton"><span>Сделай предзаказ<br> на starcraft 2 hots</span></a>
			</div>
		</div>
		<!-- ///Задий фон -->
		<!-- Страничка -->
		<!-- HEADER -->
		<div class="wrapper">
			<!-- Верхнее меню -->	
			<div id="ravn">
				<div id="top-menu" >
					<?
					$APPLICATION->IncludeComponent('bitrix:menu', "top_horizontal", array(
							"ROOT_MENU_TYPE" => "top",
							"MENU_CACHE_TYPE" => "Y",
							"MENU_CACHE_TIME" => "36000000",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"MENU_CACHE_GET_VARS" => array(),
							"MAX_LEVEL" => "1",
							"USE_EXT" => "N",
							"ALLOW_MULTI_SELECT" => "N"
						)
					);
					?>	
				</div>
			</div>
			<!-- ///Верхнее меню  -->
			<!--  контакты - хидер  -->
			<div id="header-cont">
				<div id="header-logo">
					<a href="/">
						<img src="<?= SITE_TEMPLATE_PATH ?>/images/logo1.jpg" alt="logo1">
					</a>
				</div>
				<div id="header-authorisation">
					<?
					$APPLICATION->IncludeComponent("bitrix:system.auth.form", "eshop", array(
							"REGISTER_URL" => SITE_DIR . "login/",
							"PROFILE_URL" => SITE_DIR . "personal/",
							"SHOW_ERRORS" => "N"
						), false, Array()
					);
					?>
				</div>
				<div id="header-adress">
					<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH."/s1/include/header_address.php"), false);?>
				</div>
				<div id="header-contacts">
					<p>звони!</p>
					<p>
						<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH."/s1/include/header_phone.php"), false);?>
					</p>
					<a id="bz" href="javascript:void(0);">заказать звонок</a>
					<?$APPLICATION->IncludeComponent(
						"bitrix:main.feedback",
						"modal",
						Array(
							"USE_CAPTCHA" => "Y",
							"OK_TEXT" => "Спасибо, ваше сообщение принято.",
							"EMAIL_TO" => "sale@arcade.ya1.ru",
							"REQUIRED_FIELDS" => array(),
							"EVENT_MESSAGE_ID" => array()
						),
					false
					);?>						
					<a href="mailto:sale@arcade.ya1.ru">sale@arcade.ya1.ru</a>
				</div>
			</div>
			<!-- /// контакты -хидер  -->
			<!-- ///////HEADER -->
			<!-- Левое меню + контент -->
			<div id="lmen-cont">
				<!-- Левое меню -->
				<div id="left-menu">				
					<?
					$APPLICATION->IncludeComponent("bitrix:menu", "tree_vertical", array(
						"ROOT_MENU_TYPE" => "left",
						"MENU_CACHE_TYPE" => "A",
						"MENU_CACHE_TIME" => "36000000",
						"MENU_CACHE_USE_GROUPS" => "Y",
						"MENU_CACHE_GET_VARS" => array(
						),
						"MAX_LEVEL" => "4",
						"CHILD_MENU_TYPE" => "left",
						"USE_EXT" => "Y",
						"DELAY" => "N",
						"ALLOW_MULTI_SELECT" => "N"
						),
						false
					);
					?>
					<!-- В контакте -->
					<div class="vk">
						<p>Мы вконтакте</p>
						<script type="text/javascript" src="//vk.com/js/api/openapi.js?101"></script>
						<!-- VK Widget -->
						<div id="vk_groups"></div>
						<script type="text/javascript">
							VK.Widgets.Group("vk_groups", {mode: 0, width: "213", height: "290", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 37701246);
						</script>
					</div>
					<!-- ////В контакте -->
				</div>

				<!-- ///Левое меню -->
				<!-- Баннеры + контент -->
				<div id="cont-ban">